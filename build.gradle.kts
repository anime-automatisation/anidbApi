import buildVersions.PluginVersion.javaVersion
import de.comhix.gradle.plugins.version.Version
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

project.group = "de.comhix"
project.version = Version(0, 10)

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${buildVersions.DependencyVersions.kotlinSerializationVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${buildVersions.DependencyVersions.kotlinCoroutineVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:${buildVersions.DependencyVersions.kotlinTimeVersion}")
    implementation("de.comhix.commons:updater:${buildVersions.DependencyVersions.commonsVersion}")
    implementation("de.comhix.commons:logging:${buildVersions.DependencyVersions.commonsVersion}")
    implementation("com.google.guava:guava:31.0.1-jre")
    implementation("com.google.code.gson:gson:2.9.0")
    implementation("com.squareup.okhttp3:okhttp:4.9.3")
    implementation("javax.xml.bind:jaxb-api:2.4.0-b180830.0359")
    testImplementation("org.amshove.kluent:kluent:${buildVersions.DependencyVersions.kluentVersion}")
    testImplementation("io.mockk:mockk:${buildVersions.DependencyVersions.mockkVersion}")
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-testng"))
    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo:3.3.1")
}

plugins {
    kotlin("jvm") version buildVersions.PluginVersion.kotlin
    kotlin("plugin.serialization") version buildVersions.PluginVersion.kotlin
    id("jacoco")
    id("maven-publish")
    id("signing")
    id("org.jetbrains.dokka") version buildVersions.PluginVersion.dokka
    id("org.sonatype.gradle.plugins.scan") version buildVersions.PluginVersion.ossScan
    id("de.comhix.gradle.plugins.version") version buildVersions.PluginVersion.version
}

jacoco.toolVersion = buildVersions.PluginVersion.jacoco

sourceSets {
    main {
        java {
            srcDir("${project.projectDir}/src/generated/kotlin")
        }
    }
}

tasks.compileKotlin {
    kotlinOptions.jvmTarget = javaVersion.toString()
}
tasks.compileTestKotlin {
    kotlinOptions.jvmTarget = javaVersion.toString()
}

java {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

tasks.test {
    useTestNG()
}

tasks.jar {
    manifest {
        attributes["Implementation-Version"] = project.version
    }
    destinationDirectory.set(file("$buildDir/output"))
    archiveFileName.set("${project.name}-${project.version}.${archiveExtension.get()}")
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        csv.required.set(false)
        html.outputLocation.set(file("${buildDir}/reports/jacocoHtml"))
    }
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

val generateSources: Task by tasks.creating {
    outputs.dir("${project.projectDir}/src/generated/kotlin")

    doFirst {
        generateFile(
            "${project.group}.${project.name}.info",
            "Version.kt",
            """object Version{
    const val BUILD_VERSION = "${project.version}"
    const val BUILD_DATE = "${Date()}"
    const val BUILD_TIMESTAMP = ${Date().time}
}
"""
        )
    }
}

fun generateFile(packageName: String, fileName: String, fileContent: String) {
    val outputDir: File = file("${project.projectDir}/src/generated/kotlin/${packageName.replace(".", "/")}")
    if (!outputDir.exists()) outputDir.mkdirs()

    Files.write(
        Paths.get(outputDir.absolutePath, fileName), """package $packageName

$fileContent
""".toByteArray()
    )
}

tasks.compileKotlin {
    dependsOn(generateSources)
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    destinationDirectory.set(file("$buildDir/output"))
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
    destinationDirectory.set(file("$buildDir/output"))
}

tasks.register<JacocoReport>("codeCoverageReport") {
    group = "reporting"

//    executionData(fileTree(project.rootDir.absolutePath).include("**/build/jacoco/*.exec"))
    //    sourceSets(project.extensions.getByType(SourceSetContainer::class.java).getByName("main"))
    sourceSets(
        *allprojects.filterNot { it == it.rootProject }
            .mapNotNull {
                println("sourcesset of ${it.name}")
                it.extensions.findByType(SourceSetContainer::class.java)?.findByName("main")
            }
            .toTypedArray()
    )

    reports {
        csv.required.set(false)
        xml.required.set(true)
        html.required.set(true)
        xml.outputLocation.set(file("$buildDir/jacoco/jacoco.xml"))
        html.outputLocation.set(file("${buildDir}/reports/jacocoHtml"))
    }

    val dependsOn: Set<String> = setOf("test", "jacocoTestReport")
    val runAfter: Set<String> = setOf(
        "compileKotlin",
        "copyToLib",
        "productionExecutableCompileSync",
        "browserProductionWebpack",
        "compileCommonMainKotlinMetadata",
        "allTests",
        "assemble"
    )

    dependsOn(allprojects
                  .flatMap { subProject ->
                      dependsOn.mapNotNull { subProject.tasks.findByName(it) }
                  })
    mustRunAfter(allprojects
                     .flatMap { subProject ->
                         runAfter.mapNotNull { subProject.tasks.findByName(it) }
                     })
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
            artifact(dokkaJar)
            artifact(sourcesJar)
        }
    }
    repositories {
        maven {
            url = uri("$buildDir/repository")
        }
    }
}

ossIndexAudit {
    isShowAll = false
    isAllConfigurations = false
    isPrintBanner = false
    outputFormat = org.sonatype.gradle.plugins.scan.ossindex.OutputFormat.DEPENDENCY_GRAPH
}

signing {
    isRequired = !project.version.toString().endsWith("SNAPSHOT")
}
