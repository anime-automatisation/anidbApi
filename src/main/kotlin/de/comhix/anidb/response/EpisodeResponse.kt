package de.comhix.anidb.response

import de.comhix.anidb.data.Episode
import de.comhix.anidb.data.EpisodeType
import de.comhix.anidb.data.EpisodeType.*

/**
 * @author Benjamin Beeker
 */
class EpisodeResponse(response: String) : Response(response) {

    val episode: Episode?

    init {
        if (code == 340) {
            episode = null
        }
        else {
            val data = response.substring("240 EPISODE ".length)
            val iterator = data.split("|").iterator()
            episode = Episode(
                id = iterator.read()!!,
                animeId = iterator.read(),
                length = iterator.read(),
                rating = iterator.read(),
                votes = iterator.read(),
                typedEpisodeNumber = iterator.read(),
                englishName = iterator.read(),
                romajiName = iterator.read(),
                kanjiName = iterator.read(),
                airedDate = iterator.read(),
                episodeType = iterator.specialRead(this::typeReader)
            )
        }
    }

    private fun typeReader(typeString: String): EpisodeType {
        return when (typeString) {
            "1"  -> REGULAR
            "2"  -> SPECIAL
            "3"  -> CREDIT
            "4"  -> TRAILER
            "5"  -> PARODY
            else -> OTHER
        }
    }
}