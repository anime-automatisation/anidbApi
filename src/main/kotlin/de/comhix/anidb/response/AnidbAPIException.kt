@file:Suppress("unused")

package de.comhix.anidb.response

/**
 * @author Benjamin Beeker
 */
class AnidbAPIException(val code: Int, val anidbMessage: String) : Exception("$code - $anidbMessage") {
    val error = AnidbAPIError.byCode(code)!!

    constructor(errorResponse: ErrorResponse) : this(errorResponse.code, errorResponse.message)
}

enum class AnidbAPIError(val code: Int) {
    ILLEGAL_INPUT_OR_ACCESS_DENIED(505),
    BANNED(555),
    UNKNOWN_COMMAND(598),
    INTERNAL_SERVER_ERROR(600),
    ANIDB_OUT_OF_SERVICE(601),
    SERVER_BUSY(602),
    TIMEOUT(604),
    LOGIN_FIRST(501),
    ACCESS_DENIED(502),
    INVALID_SESSION(506),
    LOGIN_FAILED(500),
    CLIENT_VERSION_OUTDATED(503),
    CLIENT_BANNED(504);

    companion object {
        fun byCode(code: Int): AnidbAPIError? {
            return values().find { it.code == code }
        }
    }
}