package de.comhix.anidb.response

import de.comhix.commons.logging.info
import kotlinx.datetime.Instant
import java.util.*

/**
 * @author Benjamin Beeker
 */

@Throws(IllegalArgumentException::class)
internal inline fun <reified ResponseType> Iterator<String>.getForFlag(mask: () -> Boolean): ResponseType? {
    return if (mask.invoke()) {
        read()
    }
    else {
        null
    }
}

internal inline fun <reified ResponseType> Iterator<String>.read(): ResponseType? {
    if (!hasNext())
        return null
    val stringValue = next()

    return try {
        when {
            ResponseType::class == String::class  -> stringValue
            ResponseType::class == Int::class     -> stringValue.trimZero().toInt()
            ResponseType::class == Date::class    -> Date(stringValue.trimZero().toLong() * 1000)
            ResponseType::class == Instant::class -> Instant.fromEpochSeconds(stringValue.trimZero().toLong())
            ResponseType::class == Boolean::class -> stringValue == "1"
            else                                  -> throw IllegalArgumentException("$stringValue not parsable as ${ResponseType::class}")
        } as ResponseType
    }
    catch (e: NumberFormatException) {
        info { "string: $stringValue" }
        info { "bytes: ${stringValue.toByteArray().contentToString()}" }
        throw e
    }
}

internal inline fun <ResponseType> Iterator<String>.specialRead(reader: (String) -> ResponseType?): ResponseType? {
    return if (!hasNext())
        null
    else
        reader.invoke(next())
}

@Suppress("UNCHECKED_CAST")
@Throws(IllegalArgumentException::class)
internal inline fun <reified ResponseType> Iterator<String>.getListForFlag(mask: () -> Boolean): List<ResponseType>? {
    val stringValue = (if (mask.invoke() && hasNext()) {
        next()
    }
    else {
        null
    }) ?: return null

    if (stringValue.trimZero().isEmpty()) {
        return listOf()
    }

    return try {
        when {
            ResponseType::class == String::class -> stringValue.split(",").toList()
            ResponseType::class == Int::class    -> stringValue.split(Regex("[,']"))
                .mapNotNull {
                    it.trimZero().ifBlank {
                        null
                    }
                }
                .map {
                    it.toInt()
                }
                .toList()

            else                                 -> throw IllegalArgumentException("$stringValue not parsable as ${ResponseType::class}")
        } as List<ResponseType>
    }
    catch (e: NumberFormatException) {
        info { "string: $stringValue" }
        info { "bytes: ${stringValue.toByteArray().contentToString()}" }
        throw e
    }
}

fun String.trimZero(): String {
    return this.trim(Char(0)).trim()
}