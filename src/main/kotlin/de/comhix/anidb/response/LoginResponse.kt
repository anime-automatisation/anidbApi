package de.comhix.anidb.response

import de.comhix.anidb.data.Session

/**
 * @author Benjamin Beeker
 */
class LoginResponse(response: String) : Response(response) {
    val session: Session

    init {
        if (code == 200 || code == 201) {
            session = Session(message.substringBefore(" "))
        }
        else {
            throw AnidbAPIException(code, message)
        }
    }
}