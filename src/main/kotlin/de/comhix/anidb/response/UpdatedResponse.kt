package de.comhix.anidb.response

import java.util.*

/**
 * @author Benjamin Beeker
 */
class UpdatedResponse(response: String) : Response(response) {
    val total: Int
    val lastUpdateDate: Date
    val animeIds: List<String>

    init {
        if (code == 343) {
            total = 0
            lastUpdateDate = Date()
            animeIds = listOf()
        }
        else {
            val splitted = response.split("|")
            total = splitted[1].toInt()
            lastUpdateDate = Date(splitted[2].toLong())
            animeIds = splitted[3].split(",").toList()
        }
    }
}