package de.comhix.anidb.response

import de.comhix.anidb.data.Anime
import de.comhix.anidb.request.mask.AnimeMask

/**
 * @author Benjamin Beeker
 */
class AnimeResponse(private val animeMask: AnimeMask, response: String) : Response(response) {
    val anime: Anime

    init {
        val data = response.substring("230 ANIME ".length)
        val split = data.split("|")
        val iterator = split.iterator()
        anime = Anime(
            iterator.next().toInt(),
            iterator.getForFlag(animeMask::dateFlags),
            iterator.getForFlag(animeMask::year),
            iterator.getForFlag(animeMask::type),
            iterator.getListForFlag(animeMask::relatedAnimeIds),
            iterator.getListForFlag(animeMask::relatedAnimeTypes),
            iterator.getForFlag(animeMask::romajiName),
            iterator.getForFlag(animeMask::kanjiName),
            iterator.getForFlag(animeMask::englishName),
            iterator.getForFlag(animeMask::otherName),
                      iterator.getListForFlag(animeMask::shortNames),
                      iterator.getListForFlag(animeMask::synonyms),
                      iterator.getForFlag(animeMask::episodes),
                      iterator.getForFlag(animeMask::highestEpisodeNumber),
                      iterator.getForFlag(animeMask::specialEpisodesCount),
                      iterator.getForFlag(animeMask::airDate),
                      iterator.getForFlag(animeMask::endDate),
                      iterator.getForFlag(animeMask::url),
                      iterator.getForFlag(animeMask::pictureName),
                      iterator.getForFlag(animeMask::rating),
                      iterator.getForFlag(animeMask::voteCount),
                      iterator.getForFlag(animeMask::tempRating),
                      iterator.getForFlag(animeMask::tempVoteCount),
                      iterator.getForFlag(animeMask::averageReviewRating),
                      iterator.getForFlag(animeMask::reviewCount),
                      iterator.getListForFlag(animeMask::awards),
                      iterator.getForFlag(animeMask::adult),
                      iterator.getForFlag(animeMask::annId),
                      iterator.getForFlag(animeMask::allcinemaId),
                      iterator.getForFlag(animeMask::animeNfoId),
                      iterator.getListForFlag(animeMask::tagNames),
                      iterator.getListForFlag(animeMask::tagIds),
                      iterator.getListForFlag(animeMask::tagWeights),
                      iterator.getForFlag(animeMask::recordUpdateDate),
                      iterator.getListForFlag(animeMask::characterIds),
                      iterator.getForFlag(animeMask::specialsCount),
                      iterator.getForFlag(animeMask::creditsCount),
                      iterator.getForFlag(animeMask::otherCount),
                      iterator.getForFlag(animeMask::trailerCount),
                      iterator.getForFlag(animeMask::parodyCount))
    }
}