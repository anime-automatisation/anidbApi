package de.comhix.anidb.response

import de.comhix.anidb.data.Anime
import de.comhix.anidb.data.File
import de.comhix.anidb.request.mask.FileAnimeMask
import de.comhix.anidb.request.mask.FileMask
import de.comhix.anidb.request.FileRequest

/**
 * @author Benjamin Beeker
 */
class FileResponse(request: FileRequest, response: String) : Response(response) {
    val file: File?
    val anime: Anime?

    init {
        if (response == "320 NO SUCH FILE") {
            file = null
            anime = null
        }
        else {
            val responseParts = response.substring("220 FILE ".length).split("|").iterator()
            file = FileBuilder(responseParts, request.fileMask).build()
            anime = AnimeBuilder(responseParts, request.fileAnimeMask).build(0)
        }
    }

    class FileBuilder(private val iterator: Iterator<String>, private val fileMask: FileMask) {
        fun build(): File {
            val fid = iterator.next().toInt()
            val aid: Int? = iterator.getForFlag(fileMask::animeId)
            val eid: Int? = iterator.getForFlag(fileMask::episodeId)
            val gid: Int? = iterator.getForFlag(fileMask::groupId)
            iterator.getForFlag<String?>(fileMask::mylistId)
            iterator.getForFlag<String?>(fileMask::otherEpisodes)
            iterator.getForFlag<String?>(fileMask::deprecated)
            val state: Int? = iterator.getForFlag(fileMask::state)
            iterator.getForFlag<String?>(fileMask::size)
            val ed2k: String? = iterator.getForFlag(fileMask::ed2k)
            iterator.getForFlag<String?>(fileMask::md5)
            iterator.getForFlag<String?>(fileMask::sha1)
            val crc: String? = iterator.getForFlag(fileMask::crc32)
            iterator.getForFlag<String?>(fileMask::videoColorDepth)
            iterator.getForFlag<String?>(fileMask::quality)
            val source: String? = iterator.getForFlag(fileMask::source)
            iterator.getForFlag<String?>(fileMask::audioCodecs)
            iterator.getForFlag<String?>(fileMask::audioBitrates)
            iterator.getForFlag<String?>(fileMask::videoCodec)
            iterator.getForFlag<String?>(fileMask::videoBitrate)
            iterator.getForFlag<String?>(fileMask::videoResolution)
            val fileType: String? = iterator.getForFlag(fileMask::fileType)
            iterator.getForFlag<String?>(fileMask::dubLanguage)
            iterator.getForFlag<String?>(fileMask::subLanguage)
            iterator.getForFlag<String?>(fileMask::lengthInSeconds)
            iterator.getForFlag<String?>(fileMask::description)
            iterator.getForFlag<String?>(fileMask::airedDate)
            iterator.getForFlag<String?>(fileMask::anidbFileName)
            iterator.getForFlag<String?>(fileMask::mylistState)
            iterator.getForFlag<String?>(fileMask::mylistFilestate)
            iterator.getForFlag<String?>(fileMask::mylistViewed)
            iterator.getForFlag<String?>(fileMask::mylistViewdate)
            iterator.getForFlag<String?>(fileMask::mylistStorage)
            iterator.getForFlag<String?>(fileMask::mylistSource)
            iterator.getForFlag<String?>(fileMask::mylistOther)
            return File(fid, aid, eid, ed2k, crc, source, fileType, state)
        }
    }

    class AnimeBuilder(private val iterator: Iterator<String>, private val fileAnimeMask: FileAnimeMask) {
        fun build(aid: Int): Anime {
            val episodes: Int? = iterator.getForFlag(fileAnimeMask::animeTotalEpisodes)
            val episodeCount: Int? = iterator.getForFlag(fileAnimeMask::highestEpisodeNumber)
            val year: String? = iterator.getForFlag(fileAnimeMask::year)
            val type: String? = iterator.getForFlag(fileAnimeMask::type)
            val relatedAnimeIds: List<Int>? = iterator.getListForFlag(fileAnimeMask::relatedAnimeIds)
            val relatedAnimeTypes: List<Int>? = iterator.getListForFlag(fileAnimeMask::relatedAnimeTypes)
            iterator.getForFlag<String?>(fileAnimeMask::categories)
            val titleRomaji: String? = iterator.getForFlag(fileAnimeMask::romajiName)
            val titleKanji: String? = iterator.getForFlag(fileAnimeMask::kanjiName)
            val titleEnglish: String? = iterator.getForFlag(fileAnimeMask::englishName)
            val other: String? = iterator.getForFlag(fileAnimeMask::otherName)
            val shortNames: List<String>? = iterator.getListForFlag(fileAnimeMask::shortNames)
            val synonyms: List<String>? = iterator.getListForFlag(fileAnimeMask::synonyms)
            iterator.getForFlag<String?>(fileAnimeMask::episodeNumber)
            iterator.getForFlag<String?>(fileAnimeMask::episodeName)
            iterator.getForFlag<String?>(fileAnimeMask::episodeRomajiName)
            iterator.getForFlag<String?>(fileAnimeMask::episodeKanjiName)
            iterator.getForFlag<String?>(fileAnimeMask::episodeRating)
            iterator.getForFlag<String?>(fileAnimeMask::episodeVoteCount)
            iterator.getForFlag<String?>(fileAnimeMask::groupName)
            iterator.getForFlag<String?>(fileAnimeMask::groupShortName)
            iterator.getForFlag<String?>(fileAnimeMask::recordUpdatedDate)

            return Anime(aid,
                         episodes = episodes,
                         highestEpisodeNumber = episodeCount,
                         year = year,
                         type = type,
                         relatedAnimeIds = relatedAnimeIds,
                         relatedAnimeTypes = relatedAnimeTypes,
                         romajiName = titleRomaji,
                         kanjiName = titleKanji,
                         englishName = titleEnglish,
                         otherName = other,
                         shortNames = shortNames,
                         synonyms = synonyms)
        }
    }
}