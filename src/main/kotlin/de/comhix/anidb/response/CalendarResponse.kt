package de.comhix.anidb.response

import java.util.*

/**
 * @author Benjamin Beeker
 */
class CalendarResponse(response: String) : Response(response) {
    val calendarEntries: List<CalendarEntry>

    init {
        calendarEntries = if (code == 397) {
            listOf()
        }
        else {
            val lines = message.split("\n").map(String::trimZero).filter(String::isNotEmpty)
            lines.subList(1, lines.size)
                    .map { it.split("|") }
                    .map { CalendarEntry(it[0].toInt(), Date(it[1].toLong()), it[2].toInt()) }
                    .toList()
        }
    }
}

data class CalendarEntry(val animeId: Int, val startDate: Date, val flags: Int)