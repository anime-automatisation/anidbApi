package de.comhix.anidb.response

/**
 * @author Benjamin Beeker
 */
class StringResponse(val response: String) : Response(response)