package de.comhix.anidb.response

import de.comhix.anidb.request.AnimeRequest
import de.comhix.anidb.request.FileRequest
import de.comhix.anidb.request.Request

/**
 * @author Benjamin Beeker
 */
abstract class Response(response: String) {
    val code: Int = response.substring(0, 3).toInt()
    val message: String = response.substring(4)
}

inline fun <reified ResponseType : Response> getFromResponseString(request: Request<ResponseType>, response: String): ResponseType {
    @Suppress("UNCHECKED_CAST")
    return when (ResponseType::class) {
        StringResponse::class   -> StringResponse(response)
        LoginResponse::class    -> LoginResponse(response)
        FileResponse::class     -> FileResponse(request as FileRequest, response)
        CalendarResponse::class -> CalendarResponse(response)
        UpdatedResponse::class  -> UpdatedResponse(response)
        EpisodeResponse::class  -> EpisodeResponse(response)
        AnimeResponse::class    -> AnimeResponse((request as AnimeRequest).mask, response)
        else                    -> throw IllegalStateException("Can not create response ${ResponseType::class}")
    } as ResponseType
}