package de.comhix.anidb.response

/**
 * @author Benjamin Beeker
 */
class ErrorResponse(response: String) : Response(response)