package de.comhix.anidb

import de.comhix.anidb.data.Session
import de.comhix.anidb.request.LoginRequest
import de.comhix.anidb.request.Request
import de.comhix.anidb.response.AnidbAPIException
import de.comhix.anidb.response.ErrorResponse
import de.comhix.anidb.response.Response
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

/**
 * @author Benjamin Beeker
 */
class SessionCommandDispatcher(val dispatcher: CommandDispatcher, loginRequest: LoginRequest) {
    var session: Session? = null
        private set

    init {
        dispatcher.enqueue(loginRequest, { throw AnidbAPIException(it) }, { throw it }) {
            session = it.session
        }
    }

    fun sessionExists(): Boolean = session == null

    fun checkErrorForSessionResponse(errorResponse: ErrorResponse) {
        if (errorResponse.code == 501 || errorResponse.code == 506) {
            session = null
        }
    }

    inline fun <reified Type : Response> createSessionRequest(request: Request<Type>): Request<Type> {
        if (request.command.contains("s=")) {
            return request
        }
        val sessionString = (if (request.command.contains(" ")) "&" else " ") + "s=${session!!.id}"

        return object : Request<Type>(request.command + sessionString) {}
    }

    inline fun <reified Type : Response> enqueue(request: Request<Type>): Deferred<Type> {
        if (!sessionExists()) {
            val deferred: CompletableDeferred<Type> = CompletableDeferred()
            deferred.completeExceptionally(AnidbAPIException(501, "LOGIN FIRST"))
            return deferred
        }
        return dispatcher.enqueue { createSessionRequest(request) }
    }

    inline fun <reified Type : Response> enqueue(
        request: Request<Type>,
        noinline onException: (Exception) -> Unit,
        crossinline handler: (Type) -> Unit
    ) {
        if (!sessionExists()) {
            onException.invoke(AnidbAPIException(501, "LOGIN FIRST"))
            return
        }

        dispatcher.enqueue(
            { createSessionRequest(request) },
            {
                checkErrorForSessionResponse(it)
                onException(AnidbAPIException(it))
            },
            onException,
            handler
        )
    }
}