package de.comhix.anidb.request

import de.comhix.anidb.data.Session
import de.comhix.anidb.request.mask.FileAnimeMask
import de.comhix.anidb.request.mask.FileMask
import de.comhix.anidb.response.FileResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#FILE:_Retrieve_File_Data)
 */
class FileRequest(query: String, val fileMask: FileMask, val fileAnimeMask: FileAnimeMask, session: Session) :
        Request<FileResponse>("FILE $query&fmask=$fileMask&amask=$fileAnimeMask&s=${session.id}") {

    constructor(size: Long, ed2k: String, fileMask: FileMask, fileAnimeMask: FileAnimeMask, session: Session) :
            this("size=$size&ed2k=$ed2k", fileMask, fileAnimeMask, session)

    constructor(fileId: Int, fileMask: FileMask, fileAnimeMask: FileAnimeMask, session: Session) :
            this("fid=$fileId", fileMask, fileAnimeMask, session)
}