package de.comhix.anidb.request

import de.comhix.anidb.data.Session
import de.comhix.anidb.response.StringResponse
import java.util.*

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#MYLISTADD:_Add_file_to_MyList)
 */
class MylistAddRequest(fileId: String, viewdate: Date, state: Int, session: Session) :
        Request<StringResponse>("MYLISTADD",
                                Pair("fid", fileId),
                                Pair("viewdate", viewdate.time.let { (it / 1000) }.toString()),
                                Pair("viewed", "1"),
                                Pair("state", state.toString()),
                                Pair("s", session.id))

enum class MylistState(val stateValue: Int) {
    UNKNOWN(0),
    INTERNAL_STORAGE(1),
    EXTERNAL_STORAGE(2),
    DELETED(3),
    REMOTE_STORAGE(4)
}

enum class FileState(val stateValue: Int) {
    NORMAL(0),
    CORRUPTED(1),
    SELF_EDITED(2),
    SELF_RIPPED(10),
    ON_DVD(11),
    ON_VHS(12),
    ON_TV(13),
    IN_THEATERS(14),
    STREAMED(15),
    OTHER(100)
}