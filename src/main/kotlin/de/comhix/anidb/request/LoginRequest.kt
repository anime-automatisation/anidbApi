package de.comhix.anidb.request

import de.comhix.anidb.response.LoginResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#AUTH:_Authing_to_the_AnimeDB)
 */
class LoginRequest(username: String, password: String, client: String, clientVersion: Int) :
    Request<LoginResponse>("AUTH user=$username&pass=$password&protover=3&client=$client&clientver=$clientVersion")