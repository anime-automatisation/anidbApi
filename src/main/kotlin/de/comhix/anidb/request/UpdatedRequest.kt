package de.comhix.anidb.request

import de.comhix.anidb.response.UpdatedResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#UPDATED:_Get_List_of_Updated_Anime_IDs)
 */
class UpdatedRequest(query: String) : Request<UpdatedResponse>("UPDATED entity=1&$query")