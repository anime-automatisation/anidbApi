package de.comhix.anidb.request

import de.comhix.anidb.data.Session
import de.comhix.anidb.response.StringResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#UPTIME:_Retrieve_Server_Uptime)
 */
class UptimeRequest(session: Session) : Request<StringResponse>("UPTIME", Pair("s", session.id))