package de.comhix.anidb.request

import de.comhix.anidb.data.Session
import de.comhix.anidb.response.CalendarResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#CALENDAR:_Get_Upcoming_Titles)
 */
class CalendarRequest(session: Session) : Request<CalendarResponse>("CALENDAR s=${session.id}")