package de.comhix.anidb.request.mask

/**
 * @author Benjamin Beeker
 */
class FileMask : Mask(5) {
    var animeId: Boolean
        get() = this[0, 6]
        set(value) {
            this[0, 6] = value
        }
    var episodeId: Boolean
        get() = this[0, 5]
        set(value) {
            this[0, 5] = value
        }
    var groupId: Boolean
        get() = this[0, 4]
        set(value) {
            this[0, 4] = value
        }
    var mylistId: Boolean
        get() = this[0, 3]
        set(value) {
            this[0, 3] = value
        }
    var otherEpisodes: Boolean
        get() = this[0, 2]
        set(value) {
            this[0, 2] = value
        }

    var deprecated: Boolean
        get() = this[0, 1]
        set(value) {
            this[0, 1] = value
        }
    var state: Boolean
        get() = this[0, 0]
        set(value) {
            this[0, 0] = value
        }
    var size: Boolean
        get() = this[1, 7]
        set(value) {
            this[1, 7] = value
        }
    var ed2k: Boolean
        get() = this[1, 6]
        set(value) {
            this[1, 6] = value
        }
    var md5: Boolean
        get() = this[1, 5]
        set(value) {
            this[1, 5] = value
        }
    var sha1: Boolean
        get() = this[1, 4]
        set(value) {
            this[1, 4] = value
        }
    var crc32: Boolean
        get() = this[1, 3]
        set(value) {
            this[1, 3] = value
        }
    var videoColorDepth: Boolean
        get() = this[1, 1]
        set(value) {
            this[1, 1] = value
        }
    var quality: Boolean
        get() = this[2, 7]
        set(value) {
            this[2, 7] = value
        }
    var source: Boolean
        get() = this[2, 6]
        set(value) {
            this[2, 6] = value
        }
    var audioCodecs: Boolean
        get() = this[2, 5]
        set(value) {
            this[2, 5] = value
        }
    var audioBitrates: Boolean
        get() = this[2, 4]
        set(value) {
            this[2, 4] = value
        }
    var videoCodec: Boolean
        get() = this[2, 3]
        set(value) {
            this[2, 3] = value
        }
    var videoBitrate: Boolean
        get() = this[2, 2]
        set(value) {
            this[2, 2] = value
        }
    var videoResolution: Boolean
        get() = this[2, 1]
        set(value) {
            this[2, 1] = value
        }
    var fileType: Boolean
        get() = this[2, 0]
        set(value) {
            this[2, 0] = value
        }
    var dubLanguage: Boolean
        get() = this[3, 7]
        set(value) {
            this[3, 7] = value
        }
    var subLanguage: Boolean
        get() = this[3, 6]
        set(value) {
            this[3, 6] = value
        }
    var lengthInSeconds: Boolean
        get() = this[3, 5]
        set(value) {
            this[3, 5] = value
        }
    var description: Boolean
        get() = this[3, 4]
        set(value) {
            this[3, 4] = value
        }
    var airedDate: Boolean
        get() = this[3, 3]
        set(value) {
            this[3, 3] = value
        }
    var anidbFileName: Boolean
        get() = this[3, 0]
        set(value) {
            this[3, 0] = value
        }
    var mylistState: Boolean
        get() = this[4, 7]
        set(value) {
            this[4, 7] = value
        }
    var mylistFilestate: Boolean
        get() = this[4, 6]
        set(value) {
            this[4, 6] = value
        }
    var mylistViewed: Boolean
        get() = this[4, 5]
        set(value) {
            this[4, 5] = value
        }
    var mylistViewdate: Boolean
        get() = this[4, 4]
        set(value) {
            this[4, 4] = value
        }
    var mylistStorage: Boolean
        get() = this[4, 3]
        set(value) {
            this[4, 3] = value
        }
    var mylistSource: Boolean
        get() = this[4, 2]
        set(value) {
            this[4, 2] = value
        }
    var mylistOther: Boolean
        get() = this[4, 1]
        set(value) {
            this[4, 1] = value
        }
}