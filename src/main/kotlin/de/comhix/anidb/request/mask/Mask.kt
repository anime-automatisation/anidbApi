package de.comhix.anidb.request.mask

import kotlin.math.pow
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.memberProperties

/**
 * @author Benjamin Beeker
 */
abstract class Mask(byteCount: Int) {
    private val bytes: MutableList<Int> = IntRange(0, byteCount - 1).map { 0 }.toMutableList()

    operator fun get(byte: Int, position: Int): Boolean {
        val bitValue = 2.0.pow(position).toInt()
        return bytes[byte] and bitValue != 0
    }

    internal operator fun set(byte: Int, position: Int, value: Boolean) {
        if (value == get(byte, position)) {
            return
        }
        val bitValue = 2.0.pow(position).toInt()
        val change = if (value) bitValue else bitValue * -1
        bytes[byte] = bytes[byte] + change
    }

    override fun toString(): String {
        return bytes.joinToString("") { String.format("%02x", it) }
    }
}

class MaskBuilder<MaskType : Mask>(clazz: KClass<MaskType>) {
    private val mask = clazz.createInstance()

    fun set(field: KMutableProperty1<MaskType, Boolean>): MaskBuilder<MaskType> {
        field.set(mask, true)
        return this
    }

    fun all(): MaskBuilder<MaskType> {
        mask::class.memberProperties.filterIsInstance<KMutableProperty1<MaskType, Boolean>>()
                .filter { it.annotations.find { annotation -> annotation is Deprecated } == null }
                .forEach {
                    it.set(mask, true)
                }

        return this
    }

    fun build(): MaskType = mask
}

@Suppress("FunctionName")
inline fun <reified MaskType : Mask> MaskBuilder(): MaskBuilder<MaskType> = MaskBuilder(
        MaskType::class)