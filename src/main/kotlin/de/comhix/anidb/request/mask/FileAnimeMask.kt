package de.comhix.anidb.request.mask

/**
 * @author Benjamin Beeker
 */
class FileAnimeMask : Mask(4) {

    var animeTotalEpisodes: Boolean
        get() = this[0, 7]
        set(value) {
            this[0, 7] = value
        }
    var highestEpisodeNumber: Boolean
        get() = this[0, 6]
        set(value) {
            this[0, 6] = value
        }
    var year: Boolean
        get() = this[0, 5]
        set(value) {
            this[0, 5] = value
        }
    var type: Boolean
        get() = this[0, 4]
        set(value) {
            this[0, 4] = value
        }
    var relatedAnimeIds: Boolean
        get() = this[0, 3]
        set(value) {
            this[0, 3] = value
        }
    var relatedAnimeTypes: Boolean
        get() = this[0, 2]
        set(value) {
            this[0, 2] = value
        }
    var categories: Boolean
        get() = this[0, 1]
        set(value) {
            this[0, 1] = value
        }
    var romajiName: Boolean
        get() = this[1, 7]
        set(value) {
            this[1, 7] = value
        }
    var kanjiName: Boolean
        get() = this[1, 6]
        set(value) {
            this[1, 6] = value
        }
    var englishName: Boolean
        get() = this[1, 5]
        set(value) {
            this[1, 5] = value
        }
    var otherName: Boolean
        get() = this[1, 4]
        set(value) {
            this[1, 4] = value
        }
    var shortNames: Boolean
        get() = this[1, 3]
        set(value) {
            this[1, 3] = value
        }
    var synonyms: Boolean
        get() = this[1, 2]
        set(value) {
            this[1, 2] = value
        }
    var episodeNumber: Boolean
        get() = this[2, 7]
        set(value) {
            this[2, 7] = value
        }
    var episodeName: Boolean
        get() = this[2, 6]
        set(value) {
            this[2, 6] = value
        }
    var episodeRomajiName: Boolean
        get() = this[2, 5]
        set(value) {
            this[2, 5] = value
        }
    var episodeKanjiName: Boolean
        get() = this[2, 4]
        set(value) {
            this[2, 4] = value
        }
    var episodeRating: Boolean
        get() = this[2, 3]
        set(value) {
            this[2, 3] = value
        }
    var episodeVoteCount: Boolean
        get() = this[2, 2]
        set(value) {
            this[2, 2] = value
        }
    var groupName: Boolean
        get() = this[3, 7]
        set(value) {
            this[3, 7] = value
        }
    var groupShortName: Boolean
        get() = this[3, 6]
        set(value) {
            this[3, 6] = value
        }
    var recordUpdatedDate: Boolean
        get() = this[3, 0]
        set(value) {
            this[3, 0] = value
        }
}