package de.comhix.anidb.request.mask

/**
 * @author Benjamin Beeker
 */
class AnimeMask : Mask(7) {
    var animeId: Boolean
        get() = this[0, 7]
        set(value) {
            this[0, 7] = value
        }
    var dateFlags: Boolean
        get() = this[0, 6]
        set(value) {
            this[0, 6] = value
        }
    var year: Boolean
        get() = this[0, 5]
        set(value) {
            this[0, 5] = value
        }
    var type: Boolean
        get() = this[0, 4]
        set(value) {
            this[0, 4] = value
        }
    var relatedAnimeIds: Boolean
        get() = this[0, 3]
        set(value) {
            this[0, 3] = value
        }
    var relatedAnimeTypes: Boolean
        get() = this[0, 2]
        set(value) {
            this[0, 2] = value
        }
    var romajiName: Boolean
        get() = this[1, 7]
        set(value) {
            this[1, 7] = value
        }
    var kanjiName: Boolean
        get() = this[1, 6]
        set(value) {
            this[1, 6] = value
        }
    var englishName: Boolean
        get() = this[1, 5]
        set(value) {
            this[1, 5] = value
        }
    var otherName: Boolean
        get() = this[1, 4]
        set(value) {
            this[1, 4] = value
        }
    var shortNames: Boolean
        get() = this[1, 3]
        set(value) {
            this[1, 3] = value
        }
    var synonyms: Boolean
        get() = this[1, 2]
        set(value) {
            this[1, 2] = value
        }
    var episodes: Boolean
        get() = this[2, 7]
        set(value) {
            this[2, 7] = value
        }
    var highestEpisodeNumber: Boolean
        get() = this[2, 6]
        set(value) {
            this[2, 6] = value
        }

    var specialEpisodesCount: Boolean
        get() = this[2, 5]
        set(value) {
            this[2, 5] = value
        }
    var airDate: Boolean
        get() = this[2, 4]
        set(value) {
            this[2, 4] = value
        }
    var endDate: Boolean
        get() = this[2, 3]
        set(value) {
            this[2, 3] = value
        }
    var url: Boolean
        get() = this[2, 2]
        set(value) {
            this[2, 2] = value
        }
    var pictureName: Boolean
        get() = this[2, 1]
        set(value) {
            this[2, 1] = value
        }
    var rating: Boolean
        get() = this[3, 7]
        set(value) {
            this[3, 7] = value
        }
    var voteCount: Boolean
        get() = this[3, 6]
        set(value) {
            this[3, 6] = value
        }
    var tempRating: Boolean
        get() = this[3, 5]
        set(value) {
            this[3, 5] = value
        }
    var tempVoteCount: Boolean
        get() = this[3, 4]
        set(value) {
            this[3, 4] = value
        }
    var averageReviewRating: Boolean
        get() = this[3, 3]
        set(value) {
            this[3, 3] = value
        }
    var reviewCount: Boolean
        get() = this[3, 2]
        set(value) {
            this[3, 2] = value
        }
    var awards: Boolean
        get() = this[3, 1]
        set(value) {
            this[3, 1] = value
        }
    var adult: Boolean
        get() = this[3, 0]
        set(value) {
            this[3, 0] = value
        }
    var annId: Boolean
        get() = this[4, 6]
        set(value) {
            this[4, 6] = value
        }
    var allcinemaId: Boolean
        get() = this[4, 5]
        set(value) {
            this[4, 5] = value
        }
    var animeNfoId: Boolean
        get() = this[4, 4]
        set(value) {
            this[4, 4] = value
        }
    var tagNames: Boolean
        get() = this[4, 3]
        set(value) {
            this[4, 3] = value
        }
    var tagIds: Boolean
        get() = this[4, 2]
        set(value) {
            this[4, 2] = value
        }
    var tagWeights: Boolean
        get() = this[4, 1]
        set(value) {
            this[4, 1] = value
        }
    var recordUpdateDate: Boolean
        get() = this[4, 0]
        set(value) {
            this[4, 0] = value
        }
    var characterIds: Boolean
        get() = this[5, 7]
        set(value) {
            this[5, 7] = value
        }
    var specialsCount: Boolean
        get() = this[6, 7]
        set(value) {
            this[6, 7] = value
        }
    var creditsCount: Boolean
        get() = this[6, 6]
        set(value) {
            this[6, 6] = value
        }
    var otherCount: Boolean
        get() = this[6, 5]
        set(value) {
            this[6, 5] = value
        }
    var trailerCount: Boolean
        get() = this[6, 4]
        set(value) {
            this[6, 4] = value
        }
    var parodyCount: Boolean
        get() = this[6, 3]
        set(value) {
            this[6, 3] = value
        }
}