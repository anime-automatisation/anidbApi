package de.comhix.anidb.request

import de.comhix.anidb.response.StringResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#PING:_Ping_Command)
 */
class PingRequest: Request<StringResponse>("PING")