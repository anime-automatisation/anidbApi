package de.comhix.anidb.request

import de.comhix.anidb.data.Session
import de.comhix.anidb.response.StringResponse
import java.nio.charset.Charset

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#ENCODING:_Change_Encoding_for_Session)
 */
class EncodingRequest(session: Session, encoding: Charset = Charsets.UTF_8) :
    Request<StringResponse>(
        "ENCODING",
        Pair("name", encoding.name()),
        Pair("s", session.id)
    )