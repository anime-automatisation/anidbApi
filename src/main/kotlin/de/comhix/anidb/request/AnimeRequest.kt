package de.comhix.anidb.request

import de.comhix.anidb.request.mask.AnimeMask
import de.comhix.anidb.response.AnimeResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#ANIME:_Retrieve_Anime_Data)
 */
class AnimeRequest(animeId: Int, val mask: AnimeMask, sessionId: String?) :
    Request<AnimeResponse>("ANIME", Pair("aid", animeId.toString()), Pair("amask", mask.toString()), Pair("s", sessionId))