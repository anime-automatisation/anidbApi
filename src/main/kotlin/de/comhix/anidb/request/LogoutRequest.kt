package de.comhix.anidb.request

import de.comhix.anidb.response.StringResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#LOGOUT:_Logout)
 */
class LogoutRequest(sessionId: String?) : Request<StringResponse>("LOGOUT", Pair("s", sessionId))