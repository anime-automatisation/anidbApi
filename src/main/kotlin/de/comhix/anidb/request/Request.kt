package de.comhix.anidb.request

import de.comhix.anidb.response.Response
import java.net.DatagramPacket

/**
 * @author Benjamin Beeker
 */
abstract class Request<ResponseType : Response>(val command: String) {
    constructor(command: String, vararg parameters: Pair<String, String?>) :
            this(command + parameters.filter { it.second != null }.joinToString(prefix = " ", separator = "&") { it.first + "=" + it.second })

    fun toDatagramPacket(): DatagramPacket {
        val message = command
        return DatagramPacket(message.toByteArray(), message.length)
    }

    override fun toString(): String {
        return command
    }
}