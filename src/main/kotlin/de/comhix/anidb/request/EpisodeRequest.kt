package de.comhix.anidb.request

import de.comhix.anidb.response.EpisodeResponse

/**
 * @author Benjamin Beeker
 *
 * See [Api Definition](https://wiki.anidb.net/UDP_API_Definition#EPISODE:_Retrieve_Episode_Data)
 */
class EpisodeRequest(episodeId: Int, sessionId: String?) :
        Request<EpisodeResponse>("EPISODE", Pair("eid", episodeId.toString()), Pair("s", sessionId))