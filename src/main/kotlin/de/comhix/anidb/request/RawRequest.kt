package de.comhix.anidb.request

import de.comhix.anidb.response.StringResponse

/**
 * @author Benjamin Beeker
 */
class RawRequest(request: String) : Request<StringResponse>(request)