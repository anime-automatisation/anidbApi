package de.comhix.anidb

import de.comhix.anidb.request.Request
import de.comhix.anidb.response.*
import de.comhix.commons.logging.error
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.datetime.Clock
import kotlinx.datetime.Clock.System
import kotlinx.datetime.Instant
import java.net.*
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.concurrent.thread
import kotlin.time.Duration.Companion.seconds

/**
 * @author Benjamin Beeker
 */
class CommandDispatcher(localPort: Int = 55555) {
    companion object {
        private val LOG = Logger.getLogger(CommandDispatcher::class.java.name)!!
        const val REMOTE_HOST = "api.anidb.net"
        const val REMOTE_PORT = 9000
        const val REQUEST_INTERVAL = 2000L
        const val LONG_REQUEST_INTERVAL = 4000L
        const val INTERVAL_SWITCH_THRESHOLD = 10
        val SOCKET_TIMEOUT = 30.seconds.inWholeMilliseconds.toInt()
    }

    private var run = true

    private val requestQueue: Queue<() -> Unit> = LinkedBlockingQueue()
    private val socket: DatagramSocket = DatagramSocket(localPort).apply {
        soTimeout = SOCKET_TIMEOUT
    }

    var lastSuccessfulSessionCommand: Instant = Instant.fromEpochSeconds(0)
        private set

    init {
        socket.connect(InetAddress.getByName(REMOTE_HOST), REMOTE_PORT)
        thread(isDaemon = true, name = "CommandExecutor-01") {
            var idle = 0
            var busy = 0
            var interval = REQUEST_INTERVAL
            while (run) {
                if (requestQueue.isNotEmpty()) {
                    try {
                        requestQueue.poll().invoke()
                    }
                    catch (t: Throwable) {
                        LOG.log(Level.SEVERE, "unexpected throwable", t)
                    }
                    busy++
                    idle = 0
                }
                else {
                    idle++
                    busy = 0
                }
                if (idle > INTERVAL_SWITCH_THRESHOLD) {
                    interval = REQUEST_INTERVAL
                }
                else if (busy > INTERVAL_SWITCH_THRESHOLD) {
                    interval = LONG_REQUEST_INTERVAL
                }
                Thread.sleep(interval)
            }
        }
    }

    inline fun <reified Type : Response> enqueue(request: Request<Type>): Deferred<Type> = enqueue { request }

    inline fun <reified Type : Response> enqueue(request: () -> Request<Type>): Deferred<Type> {
        val deferred: CompletableDeferred<Type> = CompletableDeferred()
        enqueue(
            request(),
            { deferred.completeExceptionally(AnidbAPIException(it)) },
            { deferred.completeExceptionally(it) },
            { deferred.complete(it) })
        return deferred
    }

    inline fun <reified Type : Response> enqueue(
        request: Request<Type>,
        noinline onError: (ErrorResponse) -> Unit,
        noinline onException: (Exception) -> Unit,
        crossinline handler: (Type) -> Unit
    ) = enqueue({ request }, onError, onException, handler)

    inline fun <reified Type : Response> enqueue(
        noinline request: () -> Request<Type>,
        noinline onError: (ErrorResponse) -> Unit,
        noinline onException: (Exception) -> Unit,
        crossinline handler: (Type) -> Unit
    ) {
        enqueueUntyped(request, onError, onException) {
            val response: Type = getFromResponseString(request(), it)
            handler(response)
        }
    }

    fun enqueueUntyped(request: () -> Request<*>, onError: (ErrorResponse) -> Unit, onException: (Exception) -> Unit, handler: (String) -> Unit) {
        val execution = {
            var requestDebug: String? = null
            var responseDebug: String? = null
            try {
                val loadedRequest = request()
                requestDebug = loadedRequest.toString()
                val response = sendAndRead(loadedRequest)
                responseDebug = response
                val error = AnidbAPIError.byCode(response.substring(0, 3).toInt())

                if (error != null) {
                    if (error.code == AnidbAPIError.BANNED.code) {
                        error { "Got BANNED response, killing dispatcher" }
                        run = false
                    }
                    onError(ErrorResponse(response))
                }
                else {
                    if (loadedRequest.toString().contains(Regex("&?s=[a-zA-Z0-9]{4,8}"))) {
                        lastSuccessfulSessionCommand = System.now()
                    }
                    handler(response)
                }
            }
            catch (e: Exception) {
                val timestamp = System.now()
                requestDebug?.let {
                    Files.write(Paths.get("debug/error_${timestamp}_request.txt"), it.toByteArray())
                }
                responseDebug?.let {
                    Files.write(Paths.get("debug/error_${timestamp}_response.txt"), it.toByteArray())
                }
                onException(e)
            }
        }
        requestQueue.add(execution)
    }

    private fun read(): String {
        val buffer = ByteArray(65536)
        socket.receive(DatagramPacket(buffer, buffer.size))
        val lastIndex = buffer.indexOfLast { byte -> byte != 0.toByte() }
        return buffer.toString(Charsets.UTF_8).substring(0, lastIndex)
    }

    private fun sendAndRead(request: Request<*>): String {
        socket.send(request.toDatagramPacket())
        val response = read()
        LOG.info(request.toString())
        LOG.info(response)
        return response
    }
}