package de.comhix.anidb.util

/*
 * Java AniDB API - A Java API for AniDB.net
 * (c) Copyright 2010 grizzlyxp
 * http://anidb.net/perl-bin/animedb.pl?show=userpage&uid=63935
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.Vector

/**
 * A class that can be used to compute the Ed2k checksum of a data stream.
 *
 * @author grizzlyxp
 * (http://anidb.net/perl-bin/animedb.pl?show=userpage&uid=63935)
 * @version **1.0**, 06.01.2010
 */
/**
 * Creates an Ed2k checksum object.
 */
class Ed2kChecksum {

    /**
     * The MD4 checksum.
     */
    private val md4 = Md4Checksum()
    /**
     * The data buffer.
     */
    private val buffer = ByteArray(1024)
    /**
     * The offset.
     */
    private var bufferOffset = 0
    /**
     * The MD4 hash list.
     */
    private val hashList = Vector<ByteArray>()
    /**
     * The processed bytes.
     */
    private var count: Long = 0

    val digest: ByteArray
        get() {
            val iterator: Iterator<ByteArray>
            var digest: ByteArray
            val rest: Int

            if (this.bufferOffset != 0) {
                this.update()
            }
            rest = (this.count % CHUNK_SIZE).toInt()
            if (rest > 0) {
                this.hashList.addElement(this.md4.digest)
                this.md4.reset()
            }
            if (rest == 0) {
                this.hashList.addElement(this.md4.digest)
                this.md4.reset()
            }

            if (this.hashList.size == 0) {
                return this.md4.digest
            }
            else if (this.hashList.size == 1) {
                return this.hashList.firstElement()
            }
            iterator = this.hashList.iterator()
            while (iterator.hasNext()) {
                digest = iterator.next()
                this.md4.update(digest,
                                0,
                                digest.size)
            }
            return this.md4.digest
        }

    fun reset() {
        this.md4.reset()
        this.bufferOffset = 0
        this.hashList.removeAllElements()
        this.count = 0
    }

    private fun update() {
        this.md4.update(this.buffer,
                        0,
                        this.bufferOffset)
        this.count += this.bufferOffset.toLong()
        this.bufferOffset = 0
        if (this.count % CHUNK_SIZE == 0L) {
            this.hashList.addElement(this.md4.digest)
            this.md4.reset()
        }
    }

    fun update(buf: ByteArray?,
               off: Int,
               len: Int) {
        if (buf == null) {
            throw IllegalArgumentException("Argument buf is null.")
        }
        if (off < 0) {
            throw IllegalArgumentException(
                "Value of argument off is less than 0: $off")
        }
        if (len < 1) {
            throw IllegalArgumentException(
                "Value of argument len is less than 1: $len")
        }
        if (off + len > buf.size) {
            throw IllegalArgumentException("The sum of the values of the "
                                                   + "arguments off and len is greater than the length of "
                                                   + "argument buf: off = " + off + "; len = " + len
                                                   + "; buf.length = " + buf.size)
        }
        for (index in 0 until len) {
            this.buffer[this.bufferOffset++] = buf[off + index]
            if (this.bufferOffset == this.buffer.size) {
                this.update()
            }
        }
    }

    fun update(data: Int) {
        this.buffer[this.bufferOffset++] = data.toByte()
        if (this.bufferOffset == this.buffer.size) {
            this.update()
        }
    }

    companion object {
        /**
         * The size of a chunk.
         */
        private val CHUNK_SIZE = 9728000
    }
}

