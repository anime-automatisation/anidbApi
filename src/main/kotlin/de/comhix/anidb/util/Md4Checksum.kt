package de.comhix.anidb.util

/*
 * Java AniDB API - A Java API for AniDB.net
 * (c) Copyright 2010 grizzlyxp
 * http://anidb.net/perl-bin/animedb.pl?show=userpage&uid=63935
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * A class that can be used to compute the MD4 checksum of a data stream.
 * @author grizzlyxp
 * (http://anidb.net/perl-bin/animedb.pl?show=userpage&uid=63935)
 * @version **1.0**, 03.01.2010
 */
class Md4Checksum {

    /** The data buffer.  */
    private val buffer = ByteArray(BUFFER_SIZE)

    /** The offset.  */
    private var bufferOffset = 0

    /** Current state.  */
    private val state = IntArray(4)

    /** Count of bytes processed.  */
    private var count: Long = 0

    /** The current block.  */
    private val block = ByteArray(BLOCK_SIZE)

    val digest: ByteArray
        get() {
            val digest = ByteArray(16)

            this.finish()
            for (index in this.state.indices) {
                digest[index * 4] = this.state[index].toByte()
                digest[index * 4 + 1] = this.state[index].ushr(8)
                        .toByte()
                digest[index * 4 + 2] = this.state[index].ushr(16)
                        .toByte()
                digest[index * 4 + 3] = this.state[index].ushr(24)
                        .toByte()
            }
            return digest
        }

    /**
     * Creates a MD4 checksum object.
     */
    init {
        this.reset()
    }

    fun reset() {
        this.bufferOffset = 0
        this.state[0] = 0x67452301
        this.state[1] = -0x10325477
        this.state[2] = -0x67452302
        this.state[3] = 0x10325476
        this.count = 0
    }

    private fun transform() {
        var a: Int
        var b: Int
        var c: Int
        var d: Int
        var temp: Int
        var f: Int
        var squareRoot: Int
        var wIndex: Int
        var s: Int
        val words = IntArray(16)

        // Copy state.
        a = this.state[0]
        b = this.state[1]
        c = this.state[2]
        d = this.state[3]

        // Convert to 32 bit words.
        for (index in words.indices) {
            words[index] = (this.block[index * 4].toInt() and 0xFF
                    or (this.block[index * 4 + 1].toInt() and 0xFF shl 8)
                    or (this.block[index * 4 + 2].toInt() and 0xFF shl 16)
                    or (this.block[index * 4 + 3].toInt() shl 24))
        }

        for (index in 0..47) {
            if (index < 16) {
                f = b and c or (b.inv() and d)
                squareRoot = 0
            }
            else if (index < 32) {
                f = b and (c or d) or (c and d)

                squareRoot = SQUARE_ROOT_OF_2
            }
            else {
                f = b xor c xor d
                squareRoot = SQUARE_ROOT_OF_3
            }
            s = CONSTS[index]
            wIndex = ORDER[index]

            a += f + words[wIndex] + squareRoot
            a = a shl s or a.ushr(32 - s)

            // Rotate values.
            temp = d
            d = c
            c = b
            b = a
            a = temp
        }

        // Add to state.
        this.state[0] += a
        this.state[1] += b
        this.state[2] += c
        this.state[3] += d
    }

    private fun update() {
        var blockIndex: Int
        var bufferIndex = 0

        if (this.bufferOffset == 0) {
            return
        }
        // Calculate old block offset.
        blockIndex = (this.count % BLOCK_SIZE).toInt()
        this.count += this.bufferOffset.toLong()
        while (bufferIndex < this.bufferOffset) {
            this.block[blockIndex] = this.buffer[bufferIndex]
            blockIndex++
            if (blockIndex == BLOCK_SIZE) {
                this.transform()
                blockIndex = 0
            }
            bufferIndex++
        }
        this.bufferOffset = 0
    }

    fun update(buf: ByteArray?,
               off: Int,
               len: Int) {
        if (buf == null) {
            throw IllegalArgumentException("Argument buf is null.")
        }
        if (off < 0) {
            throw IllegalArgumentException(
                    "Value of argument off is less than 0: $off")
        }
        if (len < 1) {
            throw IllegalArgumentException(
                    "Value of argument len is less than 1: $len")
        }
        if (off + len > buf.size) {
            throw IllegalArgumentException("The sum of the values of the "
                                           + "arguments off and len is greater than the length of "
                                           + "argument buf: off = " + off + "; len = " + len
                                           + "; buf.length = " + buf.size)
        }
        for (index in 0 until len) {
            this.buffer[this.bufferOffset++] = buf[off + index]
            if (this.bufferOffset == BUFFER_SIZE) {
                this.update()
            }
        }
    }

    fun update(data: Int) {
        this.buffer[this.bufferOffset++] = data.toByte()
        if (this.bufferOffset == BUFFER_SIZE) {
            this.update()
        }
    }

    private fun finish() {
        var blockIndex: Int
        val bits: Long

        this.update()
        blockIndex = (this.count % BLOCK_SIZE).toInt()
        // Padding
        this.block[blockIndex++] = 0x80.toByte()
        if (blockIndex > 56) {
            while (blockIndex < BLOCK_SIZE) {
                this.block[blockIndex++] = 0
            }
            this.transform()
            blockIndex = 0
        }
        while (blockIndex < 56) {
            this.block[blockIndex++] = 0
        }
        bits = this.count shl 3
        this.block[blockIndex++] = bits.toByte()
        this.block[blockIndex++] = bits.ushr(8)
                .toByte()
        this.block[blockIndex++] = bits.ushr(16)
                .toByte()
        this.block[blockIndex++] = bits.ushr(24)
                .toByte()
        this.block[blockIndex++] = bits.ushr(32)
                .toByte()
        this.block[blockIndex++] = bits.ushr(40)
                .toByte()
        this.block[blockIndex++] = bits.ushr(48)
                .toByte()
        this.block[blockIndex] = bits.ushr(56)
                .toByte()
        this.transform()
    }

    companion object {
        /** The square root of 2 as hexadecimal 32-bit constant.  */
        private val SQUARE_ROOT_OF_2 = 0x5a827999

        /** The square root of 3 as hexadecimal 32-bit constant.  */
        private val SQUARE_ROOT_OF_3 = 0x6ed9eba1

        /** The constants.  */
        private val CONSTS = intArrayOf(3,
                                        7,
                                        11,
                                        19,
                                        3,
                                        7,
                                        11,
                                        19,
                                        3,
                                        7,
                                        11,
                                        19,
                                        3,
                                        7,
                                        11,
                                        19,
                                        3,
                                        5,
                                        9,
                                        13,
                                        3,
                                        5,
                                        9,
                                        13,
                                        3,
                                        5,
                                        9,
                                        13,
                                        3,
                                        5,
                                        9,
                                        13,
                                        3,
                                        9,
                                        11,
                                        15,
                                        3,
                                        9,
                                        11,
                                        15,
                                        3,
                                        9,
                                        11,
                                        15,
                                        3,
                                        9,
                                        11,
                                        15)

        /** The field order.  */
        private val ORDER = intArrayOf(0,
                                       1,
                                       2,
                                       3,
                                       4,
                                       5,
                                       6,
                                       7,
                                       8,
                                       9,
                                       10,
                                       11,
                                       12,
                                       13,
                                       14,
                                       15,
                                       0,
                                       4,
                                       8,
                                       12,
                                       1,
                                       5,
                                       9,
                                       13,
                                       2,
                                       6,
                                       10,
                                       14,
                                       3,
                                       7,
                                       11,
                                       15,
                                       0,
                                       8,
                                       4,
                                       12,
                                       2,
                                       10,
                                       6,
                                       14,
                                       1,
                                       9,
                                       5,
                                       13,
                                       3,
                                       11,
                                       7,
                                       15)

        /** The block size.  */
        private val BLOCK_SIZE = 64

        /** The buffer size.  */
        private val BUFFER_SIZE = 512
    }
}