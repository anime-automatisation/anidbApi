package de.comhix.anidb.util

import java.io.File
import java.io.InputStream
import java.util.zip.CRC32

/**
 * @author Benjamin Beeker
 */
fun File.ed2kSum(): ByteArray {
    return this.inputStream().use { it.ed2kSum() }
}

fun File.ed2kString(): String {
    val sum = this.ed2kSum()
    return sum.joinToString(separator = "") { byte ->
        String.format("%02x", byte)
    }
}

fun InputStream.ed2kSum(): ByteArray {
    val ed2k = Ed2kChecksum()
    var end = false
    while (!end) {
        val read = ByteArray(1024)
        val length = this.read(read)
        if (length <= 0) {
            break
        }
        ed2k.update(read, 0, length)
        end = read.size < 1024
    }
    this.close()
    return ed2k.digest
}

fun File.crcSum(): Long {
    return this.inputStream().use { it.crcSum() }
}

fun File.crcString(): String {
    val sum = this.crcSum()
    return String.format("%08x", sum)
}

fun InputStream.crcSum(): Long {
    val crc = CRC32()
    var end = false
    while (!end) {
        val read = ByteArray(1024)
        val length = this.read(read)
        if (length <= 0) {
            break
        }
        crc.update(read, 0, length)
        end = read.size < 1024
    }
    return crc.value
}