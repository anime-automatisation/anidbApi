package de.comhix.anidb.data

import kotlinx.datetime.Instant
import kotlinx.serialization.*
import java.util.*

/**
 * @author Benjamin Beeker
 */
@Serializable
data class Anime(
    @SerialName("_id") override val id: Int,
    val dateFlags: Int? = null,
    val year: String? = null,
    val type: String? = null,
    val relatedAnimeIds: List<Int>? = null,
    val relatedAnimeTypes: List<Int>? = null,
    val romajiName: String? = null,
    val kanjiName: String? = null,
    val englishName: String? = null,
    val otherName: String? = null,
    val shortNames: List<String>? = null,
    val synonyms: List<String>? = null,
    val episodes: Int? = null,
    val highestEpisodeNumber: Int? = null,
    val specialEpisodes: Int? = null,
    val airDate: Instant? = null,
    val endDate: Instant? = null,
    val url: String? = null,
    val picname: String? = null,
    val rating: Int? = null,
    val votes: Int? = null,
    val temporaryRating: Int? = null,
    val temporaryVotes: Int? = null,
    val averageReviewRating: Int? = null,
    val reviews: Int? = null,
    val awards: List<String>? = null,
    val adult: Boolean? = null,
    val annId: Int? = null,
    val allcinemaId: Int? = null,
    val animeNfoId: String? = null,
    val tagNames: List<String>? = null,
    val tagIds: List<Int>? = null,
    val tagWeights: List<Int>? = null,
    val recordUpdated: Instant? = null,
    val characters: List<Int>? = null,
    val specials: Int? = null,
    val credits: Int? = null,
    val others: Int? = null,
    val trailers: Int? = null,
    val parodys: Int? = null
) : StorageObject<Int> {
    override val serializationVersion: Int = 1
}

val Anime.animeId
    get() = id