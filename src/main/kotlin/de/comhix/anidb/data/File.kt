package de.comhix.anidb.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
data class File(
    @SerialName("_id") override val id: Int,
    val animeId: Int? = null,
    val episodeId: Int? = null,
    val ed2k: String? = null,
    val crc: String? = null,
    val source: String? = null,
    val fileType: String? = null,
    val state: Int? = null
) : StorageObject<Int> {
    override val serializationVersion: Int = 1
}

val File.fileId
    get() = id