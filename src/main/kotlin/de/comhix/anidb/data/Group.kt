package de.comhix.anidb.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
data class Group(@SerialName("_id") override val id: Int) : StorageObject<Int> {

    override val serializationVersion: Int = 1
}

val Group.groupId
    get() = id