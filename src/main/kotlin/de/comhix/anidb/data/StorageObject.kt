package de.comhix.anidb.data

interface StorageObject<IdType : Any> {
    val id: IdType
    val serializationVersion: Int
}