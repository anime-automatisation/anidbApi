package de.comhix.anidb.data

/**
 * @author Benjamin Beeker
 */
data class Session(val id:String)