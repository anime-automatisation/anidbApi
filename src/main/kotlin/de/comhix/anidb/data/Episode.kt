package de.comhix.anidb.data

import de.comhix.anidb.data.EpisodeType.REGULAR
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

/**
 * @author Benjamin Beeker
 */
@Serializable
data class Episode(
    @SerialName("_id") override val id: Int,
    val animeId: Int? = null,
    val length: Int? = null,
    val rating: Int? = null,
    val votes: Int? = null,
    val typedEpisodeNumber: String? = null,
    val romajiName: String? = null,
    val kanjiName: String? = null,
    val englishName: String? = null,
    val airedDate: Instant? = null,
    val episodeType: EpisodeType? = REGULAR
) : StorageObject<Int> {

    override val serializationVersion: Int = 1

    val episodeNumber: Int? = typedEpisodeNumber?.replace(Regex("[^0-9]"), "")?.toInt()
}

val Episode.episodeId
    get() = id

enum class EpisodeType {
    REGULAR,
    SPECIAL,
    CREDIT,
    TRAILER,
    PARODY,
    OTHER
}