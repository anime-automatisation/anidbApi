package de.comhix.anidb.data

import kotlinx.datetime.Instant
import kotlinx.serialization.*

/**
 * @author Benjamin Beeker
 */
@Serializable
data class LocalFile(
    @SerialName("_id") override val id: String,
    val ed2k: String? = null,
    val changed: Instant,
    val size: Long,
    val lastTry: Instant? = null
) : StorageObject<String> {
    override val serializationVersion: Int = 1
}

val LocalFile.filename
    get() = id