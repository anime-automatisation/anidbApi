package de.comhix.anidb.requestBuilder

import de.comhix.anidb.CommandDispatcher
import de.comhix.anidb.data.Session
import de.comhix.anidb.request.FileRequest
import de.comhix.anidb.request.mask.FileAnimeMask
import de.comhix.anidb.request.mask.FileMask
import de.comhix.anidb.request.mask.MaskBuilder
import de.comhix.anidb.response.FileResponse

/**
 * @author Benjamin Beeker
 */
class FileRequestBuilder(dispatcher: CommandDispatcher,
                         session: Session,
                         private val query: String) : RequestBuilderBase(dispatcher, session) {

    private lateinit var fileAnimeMask: FileAnimeMask
    private lateinit var mask: FileMask

    fun fileMask(fileMask: FileMask): FileRequestBuilder {
        this.mask = fileMask
        return this
    }

    fun animeMask(animeMask: FileAnimeMask): FileRequestBuilder {
        this.fileAnimeMask = animeMask
        return this
    }

    suspend fun call(): FileResponse {
        val fileAnimeMask = if (this::fileAnimeMask.isInitialized) fileAnimeMask
        else MaskBuilder<FileAnimeMask>()
                .all().build()
        val fileMask = if (this::mask.isInitialized) mask else MaskBuilder<FileMask>().all().build()

        return dispatcher.enqueue(FileRequest(query, fileMask, fileAnimeMask, session)).await()
    }
}

class FileRequestBuilderBase(dispatcher: CommandDispatcher, session: Session) : RequestBuilderBase(dispatcher, session) {
    fun withHash(size: Long, ed2k: String): FileRequestBuilder {
        return FileRequestBuilder(dispatcher, session, "size=$size&ed2k=$ed2k")
    }

    fun withId(fileId: Int): FileRequestBuilder {
        return FileRequestBuilder(dispatcher, session, "fid=$fileId")
    }
}
