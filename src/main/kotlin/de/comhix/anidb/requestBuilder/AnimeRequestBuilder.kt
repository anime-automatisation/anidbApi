package de.comhix.anidb.requestBuilder

import de.comhix.anidb.CommandDispatcher
import de.comhix.anidb.data.Session
import de.comhix.anidb.request.AnimeRequest
import de.comhix.anidb.request.mask.AnimeMask
import de.comhix.anidb.request.mask.MaskBuilder
import de.comhix.anidb.response.AnimeResponse

/**
 * @author Benjamin Beeker
 */
class AnimeRequestBuilder(private val animeId: Int, dispatcher: CommandDispatcher, session: Session) : RequestBuilderBase(dispatcher, session) {
    private lateinit var mask: AnimeMask

    fun animeMask(animeMask: AnimeMask): AnimeRequestBuilder {
        mask = animeMask
        return this
    }

    suspend fun call(): AnimeResponse {
        val animeMask = if (this::mask.isInitialized) mask else MaskBuilder<AnimeMask>().all().build()

        return dispatcher.enqueue(AnimeRequest(animeId, animeMask, session.id)).await()
    }
}