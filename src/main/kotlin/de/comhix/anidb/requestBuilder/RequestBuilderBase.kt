package de.comhix.anidb.requestBuilder

import de.comhix.anidb.CommandDispatcher
import de.comhix.anidb.data.Session

/**
 * @author Benjamin Beeker
 */
abstract class RequestBuilderBase (protected val dispatcher: CommandDispatcher, protected  val session: Session)