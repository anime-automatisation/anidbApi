package de.comhix.anidb

import de.comhix.anidb.data.Episode
import de.comhix.anidb.data.Session
import de.comhix.anidb.request.*
import de.comhix.anidb.requestBuilder.*
import de.comhix.anidb.response.*
import kotlinx.datetime.Clock
import java.util.*
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.ExperimentalTime

/**
 * @author Benjamin Beeker
 */
class AnidbProvider(
    private val clientId: String,
    private val clientVersion: Int,
    private val port: Int = 55555
) {
    fun get(username: String, password: String): Anidb = Anidb(username, password, clientId, clientVersion, port)
}

class Anidb internal constructor(
    private val username: String,
    private val password: String,
    private val clientId: String,
    private val clientVersion: Int,
    port: Int
) {
    private val dispatcher = CommandDispatcher(port)

    private lateinit var session: Session

    // TODO report 201 response code to client
    suspend fun login() {
        val login = dispatcher.enqueue(LoginRequest(username, password, clientId, clientVersion))
        session = login.await().session
        dispatcher.enqueue(EncodingRequest(session)).await()
    }

    suspend fun isLoggedIn(): Boolean {
        return if (::session.isInitialized) {
            if (dispatcher.lastSuccessfulSessionCommand > Clock.System.now() - 1.minutes) {
                return true
            }
            try {
                uptime()
                true
            }
            catch (e: AnidbAPIException) {
                if (e.code == AnidbAPIError.INVALID_SESSION.code ||
                    e.code == AnidbAPIError.LOGIN_FIRST.code
                ) {
                    false
                }
                else {
                    throw e
                }
            }
        }
        else {
            false
        }
    }

    suspend fun uptime() {
        dispatcher.enqueue(UptimeRequest(session)).await()
    }

    suspend fun ping() {
        dispatcher.enqueue(PingRequest()).await()
    }

    fun file(size: Long, ed2k: String): FileRequestBuilder {
        return FileRequestBuilderBase(dispatcher, session)
            .withHash(size, ed2k)
    }

    fun file(fileId: Int): FileRequestBuilder {
        return FileRequestBuilderBase(dispatcher, session)
            .withId(fileId)
    }

    fun anime(animeId: Int): AnimeRequestBuilder {
        return AnimeRequestBuilder(animeId, dispatcher, session)
    }

    suspend fun episode(episodeId: Int): Episode? {
        return dispatcher.enqueue(EpisodeRequest(episodeId, session.id))
            .await()
            .episode
    }

    suspend fun calendar(): CalendarResponse {
        return dispatcher.enqueue(CalendarRequest(session)).await()
    }

    suspend fun addToMyList(fileId: Int, viewdate: Date, state: MylistState) {
        dispatcher.enqueue(MylistAddRequest(fileId.toString(), viewdate, state.stateValue, session)).await()
    }

    @ExperimentalTime
    suspend fun updated(duration: Duration): UpdatedResponse {
        return dispatcher.enqueue(UpdatedRequest("age=${duration.inWholeDays}")).await()
    }

    suspend fun updated(after: Date): UpdatedResponse {
        return dispatcher.enqueue(UpdatedRequest("time=${after.time}")).await()
    }

    suspend fun logout() {
        if (this::session.isInitialized) {
            dispatcher.enqueue(LogoutRequest(session.id)).await()
        }
    }
}