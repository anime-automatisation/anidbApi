package de.comhix.anidb.response

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.invoking
import org.amshove.kluent.shouldThrow
import org.amshove.kluent.withMessage
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.util.logging.Logger.getLogger

/**
 * @author Benjamin Beeker
 */
object LoginResponseTest {
    private val LOG = getLogger(LoginResponseTest::class.java.name)

    @Test
    fun `session response`() {
        LOG.info("session response")

        // given
        val sessionId = "1234F"

        // when
        val response = LoginResponse("200 $sessionId LOGIN ACCEPTED")

        // then
        response.session.id `should be equal to` sessionId
    }

    @Test
    fun `session response new version available`() {
        LOG.info("session response new version available")

        // given
        val sessionId = "1234F"

        // when
        val response = LoginResponse("201 $sessionId LOGIN ACCEPTED - NEW VERSION AVAILABLE")

        // then
        response.session.id `should be equal to` sessionId
    }

    @Test(dataProvider = "failingLogin")
    fun `login failed`(code: Int, message: String) {
        LOG.info("login failed: $code - $message")

        // when
        invoking { LoginResponse("$code $message") } shouldThrow AnidbAPIException::class withMessage "$code - $message"
    }

    @DataProvider(name = "failingLogin")
    fun `failing login dataProvider`(): Array<Array<Any>> {
        return arrayOf(arrayOf(500, "LOGIN FAILED"),
                       arrayOf(503, "CLIENT VERSION OUTDATED"),
                       arrayOf(504, "CLIENT BANNED - bad client"),
                       arrayOf(505, "ILLEGAL INPUT OR ACCESS DENIED"),
                       arrayOf(601, "ANIDB OUT OF SERVICE - TRY AGAIN LATER"))
    }
}