package de.comhix.anidb.response

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be false`
import org.amshove.kluent.`should be true`
import org.testng.annotations.Test
import java.util.*
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class CalendarResponseTest {
    companion object {
        private val LOG = Logger.getLogger(CalendarResponseTest::class.java.name)!!
    }

    private val testResponse = "297 CALENDAR\n" +
                               "15204|1589587200|0\n" +
                               "15304|1590105600|0\n" +
                               "15329|1590192000|0\n" +
                               "15194|1586476800|4\n" +
                               "14623|1586131200|16\n" +
                               "\n"
    private val emptyResponse = "397 CALENDAR EMPTY"

    @Test
    fun `empty list`() {
        LOG.info("empty list")

        // given
        val response = CalendarResponse(emptyResponse)

        // when
        val entries = response.calendarEntries

        // then
        entries.isEmpty().`should be true`()
    }

    @Test
    fun `anime list`() {
        LOG.info("anime list")

        // given
        val response = CalendarResponse(testResponse)

        // when
        val entries = response.calendarEntries

        // then
        entries.isEmpty().`should be false`()
        entries[0] `should be equal to` CalendarEntry(15204, Date(1589587200), 0)
        entries[1] `should be equal to` CalendarEntry(15304, Date(1590105600), 0)
        entries[2] `should be equal to` CalendarEntry(15329, Date(1590192000), 0)
        entries[3] `should be equal to` CalendarEntry(15194, Date(1586476800), 4)
        entries[4] `should be equal to` CalendarEntry(14623, Date(1586131200), 16)
    }
}