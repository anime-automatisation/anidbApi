package de.comhix.anidb.response

import de.comhix.anidb.data.animeId
import de.comhix.anidb.request.mask.AnimeMask
import de.comhix.anidb.request.mask.MaskBuilder
import org.amshove.kluent.*
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.File
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class AnimeResponseTest {
    companion object {
        private val LOG = Logger.getLogger(AnimeResponseTest::class.java.name)!!
    }

    @Test
    fun `parse response`() {
        LOG.info("parse response")

        // given
        val mask = MaskBuilder<AnimeMask>().all().build()
        val responseString = "230 ANIME\n" +
                "673|16|2003-2003|TV Series|||Texhnolyze|TEXHNOLYZE|Texhnolyze|TEXHNOLYZE'Texhnolyze|TEX|??????'T?knol?iz'?????????|22|" +
                "22|13|1050537600|1064448000|http://www.geneon-ent.co.jp/rondorobe/anime/TEXHNOLYZE/|222274.jpg|694|3622|763|3669|745|13" +
                "||0|2304|241179|euvjof|maintenance tags,insane,boxing,martial arts,content indicators,dynamic,target audience,themes," +
                "original work,setting,elements,time,place,gunfights,swordplay,future,prostitution,sex,nudity,violence,combat,new,action," +
                "incest,sexual abuse -- TO BE SPLIT AND DELETED,science fiction,pornography,cyberpunk,disaster,sports,dystopia,tragedy," +
                "unsorted,rotten world,everybody dies,evolution,despair,melancholic,human psyche,strong male lead,downerending," +
                "dark atmosphere,dark,surreal,murder,suicide,some weird shit goin` on,mind fuck,slow-paced,end of the world," +
                "seeking protagonist,tragic ending,predominan|30,178,183,2282,2604,2605,2606,2607,2609,2610,2611,2612,2613,2619,2622," +
                "2626,2726,2748,2749,2750,2751,2797,2841,2842,2843,2846,2848,2861,2874,2881,2882,2887,2931,2952,3267,3313,3316,3328," +
                "3369,3822,3850,4084,4153,4280,4316,4340,4499,5417,5884,5952,5981,5996,6018,6149,6230,6232,6242,6247,6901,7050,7220|0,0," +
                "100,300,0,0,0,0,0,0,0,0,0,400,200,300,200,200,200,500,0,0,400,200,400,400,0,400,0,0,600,600,0,0,0,0,0,0,0,0,0,0,0,0,200," +
                "0,0,0,0,200,0,0,0,0,0,0,0,0,0,0,0|1534118391|17501,9665,9669,9667,9668,9670,17497,17498,17499,17500|8|3|0|2|0"
        val response = AnimeResponse(mask, responseString)

        // when
        val anime = response.anime

        // then
        anime.animeId `should be equal to` 673
        anime.relatedAnimeIds.`should not be null`().`should be empty`()
        anime.relatedAnimeTypes.`should not be null`().`should be empty`()
    }

    @Test
    fun `parse response 2`() {
        LOG.info("parse response 2")

        // given
        val mask = MaskBuilder<AnimeMask>()
            .set(AnimeMask::animeId)
            .set(AnimeMask::year)
            .set(AnimeMask::type)
            .set(AnimeMask::episodes)
            .set(AnimeMask::highestEpisodeNumber)
            .set(AnimeMask::specialEpisodesCount)
            .set(AnimeMask::rating)
            .set(AnimeMask::voteCount)
            .set(AnimeMask::tempRating)
            .set(AnimeMask::tempVoteCount)
            .set(AnimeMask::averageReviewRating)
            .set(AnimeMask::reviewCount)
            .set(AnimeMask::romajiName)
            .set(AnimeMask::kanjiName)
            .set(AnimeMask::englishName)
            .set(AnimeMask::otherName)
            .build()
        val responseString =
            "230 ANIME 1|1999-1999|TV Series|Seikai no Monshou|星界の紋章|Crest of the Stars|星界の紋章'Crest of the Stars|13|13|3|817|4568|821|4597|870|12"
        val response = AnimeResponse(mask, responseString)

        // when
        val anime = response.anime

        // then
        mask.toString() `should be equal to` "b0f0e0fc000000"

        anime.animeId `should be equal to` 1
        anime.year `should be equal to` "1999-1999"
        anime.type `should be equal to` "TV Series"
        anime.romajiName `should be equal to` "Seikai no Monshou"
        anime.kanjiName `should be equal to` "星界の紋章"
        anime.englishName `should be equal to` "Crest of the Stars"
        anime.otherName `should be equal to` "星界の紋章'Crest of the Stars"
        anime.highestEpisodeNumber `should be equal to` 13
        anime.episodes `should be equal to` 13
        anime.specialEpisodes `should be equal to` 3
        anime.rating `should be equal to` 817
        anime.votes `should be equal to` 4568
    }

    @Test
    fun `full anime response`() {
        LOG.info("full anime response")

        // given
        val mask = MaskBuilder<AnimeMask>().all().build()
        val responseString =
            "230 ANIME 1|16|1999-1999|TV Series|4'6'1623|1'2'61|Seikai no Monshou|星界の紋章|Crest of the Stars|星界の紋章'Crest of the Stars|" +
                    "CotS'SnM|??? ??'Hv?zdn? erb'?????|13|13|3|915321600|922579200|http://www.sunrise-inc.co.jp/seikai/|224618.jpg|817|4569|" +
                    "821|4598|870|12||0|14|90291|xzudnt|" +
                    "military,immortality,small breasts,content indicators,dynamic,themes,fetishes,original work,setting,elements,time,place," +
                    "gunfights,future,space,human enhancement,space travel,other planet,shipboard,large breasts,nudity," +
                    "slow when it comes to love,plot continuity,novel,action,science fiction,fantasy,adventure,romance,racism,disaster," +
                    "breasts,genetic modification,war,male protagonist,fictional language,car chases,storytelling,sociocultural evolution," +
                    "dialogue driven,strong female lead,space battles,boy meets girl,faster-than-light trav|36,1396,2008,2604,2605,2607,2608," +
                    "2609,2610,2611,2612,2613,2619,2626,2627,2636,2639,2660,2661,2741,2749,2778,2790,2799,2841,2846,2849,2850,2858,2868,2874," +
                    "2891,2907,2925,3040,3138,3489,3683,3929,3977,4022,4081,4440,4446,4480,4537,4665,5003,5929,6151,6230,6246,7050|300,0,200," +
                    "0,0,0,0,0,0,0,0,0,200,600,600,200,500,400,400,200,100,300,600,0,400,500,100,500,300,200,0,0,400,400,0,0,0,0,0,0,0,400,0," +
                    "0,0,0,0,0,400,0,0,0,0|1538171025|4087,7493,7501,7503,7514,7516,21911,21958,40209,49519,28,4081,4079,4080,4082,4083,4084," +
                    "4085,4086,4088,7512,21943,21944,21945,21946,21947,21948,21949,21954|0|3|0|0|0"
        val response = AnimeResponse(mask, responseString)

        // when
        val anime = response.anime

        // then
        anime.animeId `should be equal to` 1
        anime.year `should be equal to` "1999-1999"
        anime.type `should be equal to` "TV Series"
        anime.romajiName `should be equal to` "Seikai no Monshou"
        anime.kanjiName `should be equal to` "星界の紋章"
        anime.englishName `should be equal to` "Crest of the Stars"
        anime.otherName `should be equal to` "星界の紋章'Crest of the Stars"
        anime.highestEpisodeNumber `should be equal to` 13
        anime.episodes `should be equal to` 13
        anime.specialEpisodes `should be equal to` 3
        anime.rating `should be equal to` 817
        anime.votes `should be equal to` 4569
        // TODO all checks
    }

    @Test(dataProvider = "problematic anime ids")
    fun `test live anime response`(animeId: Int) {
        LOG.info { "test live anime response - $animeId" }

        // given
        val mask = MaskBuilder<AnimeMask>().all().build()
        val responseString = File("src/test/resources/anime_response_$animeId.txt").absoluteFile.readText()

        // when
        val response = AnimeResponse(mask, responseString)

        // then
        response.anime.animeId `should be equal to` animeId
    }

    @DataProvider(name = "problematic anime ids")
    fun `problematic anime ids`(): Iterator<Array<Int>> {
        return arrayOf(
            arrayOf(10648),
            arrayOf(17015)
        ).iterator()
    }
}