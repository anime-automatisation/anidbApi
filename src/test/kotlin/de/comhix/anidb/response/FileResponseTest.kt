package de.comhix.anidb.response

import de.comhix.anidb.data.*
import de.comhix.anidb.request.FileRequest
import de.comhix.anidb.request.mask.*
import org.amshove.kluent.*
import org.testng.annotations.Test
import java.util.logging.Logger.getLogger

/**
 * @author Benjamin Beeker
 */
class FileResponseTest {
    companion object {
        private val LOG = getLogger(FileResponseTest::class.java.name)!!
    }

    @Test
    fun `response to file and anime`() {
        LOG.info("response to file")

        // given
        val fileRequest = FileRequest(0,
                                      "",
                                      MaskBuilder<FileMask>().set(FileMask::episodeId).build(),
                                      MaskBuilder<FileAnimeMask>()
                                              .set(FileAnimeMask::englishName).build(),
                                      Session(""))
        val responseString = "220 FILE 1680995|35025|Texhnolyze"
        val response = FileResponse(fileRequest, responseString)

        // when
        val file = response.file
        val anime = response.anime

        // then
        file `should be equal to` File(1680995, episodeId = 35025)
        anime `should be equal to` Anime(0, englishName = "Texhnolyze")
    }

    @Test(enabled = false)
    fun `response to file and anime 2`() {
        LOG.info("response to file and anime 2")

        // given
        val fileRequest = FileRequest(0,
                                      "",
                                      MaskBuilder<FileMask>().all().build(),
                                      MaskBuilder<FileAnimeMask>().all().build(),
                                      Session(""))
        val responseString = "220 FILE\n" +
                             "1680995|673|35025|11111|294605538||0|1|1542306|4379e96604daf117361416399a8c4bd7|0e0ab20955b1b3517fda14ed7f52f7ac|cc67bbae42051aaafc9f5adc12ab4558eeeb1753|1e4974be|10|high|DVD|(HE-)AAC|75|H264/AVC|693|720x480|mkv|japanese|none|16||0|\n" +
                             "Texhnolyze - T2 - Texhnolyze DVD 04 Extra (Trailer 2) - [Hi10](1e4974be).mkv|1|0|1|1585861594||||22|22|2003-2003|TV Series|||Seinen,New,Tragedy,Dystopia,Violence,Sci-Fi,Gunfights,Action,Twisted,Cyberpunk,Sexual Abuse,Future,Martial\n" +
                             "Arts,Nudity,Swordplay,Sex,Prostitution,Incest,Boxing|Texhnolyze|TEXHNOLYZE|Texhnolyze|TEXHNOLYZE'Texhnolyze|TEX|??????'T?knol?iz'?????????|T2|Texhnolyze DVD 04 Extra (Trailer 2)|||0|0|Hi10 Anime|Hi10|1534118391"
        val response = FileResponse(fileRequest, responseString)

        // when
        val file = response.file
        val anime = response.anime

        // then
        file `should be equal to` File(1680995, 35025)
        anime `should be equal to` Anime(0, englishName = "Texhnolyze")
    }

    @Test
    fun `response to file and anime 3`() {
        LOG.info("response to file and anime 3")

        // given
        val fileRequest = FileRequest(0,
                                      "",
                                      MaskBuilder<FileMask>().all().build(),
                                      MaskBuilder<FileAnimeMask>()
                                              .set(FileAnimeMask::romajiName).build(),
                                      Session(""))
        val responseString = "220 FILE\n" +
                             "1680995|673|35025|11111|294605538||0|1|1542306|4379e96604daf117361416399a8c4bd7|0e0ab20955b1b3517fda14ed7f52f7ac|cc67bbae42051aaafc9f5adc12ab4558eeeb1753|1e4974be|10|high|DVD|(HE-)AAC|75|H264/AVC|693|720x480|mkv|japanese|none|16||0|\n" +
                             "Texhnolyze - T2 - Texhnolyze DVD 04 Extra (Trailer 2) - [Hi10](1e4974be).mkv|1|0|1|1585861594||||Texhnolyze"

        // when
        val response = FileResponse(fileRequest, responseString)

        // when
        val file = response.file
        val anime = response.anime

        // then
        file.`should not be null`()
        file.fileId `should be equal to` 1680995
        file.animeId `should be equal to` 673
        file.episodeId `should be equal to` 35025

        anime.`should not be null`()
    }

    @Test
    fun `response unknown file`() {
        LOG.info("response unknown file")

        // given
        val fileRequest = FileRequest(0,
                                      "",
                                      MaskBuilder<FileMask>().set(FileMask::episodeId).build(),
                                      MaskBuilder<FileAnimeMask>()
                                              .set(FileAnimeMask::englishName).build(),
                                      Session(""))
        val responseString = "320 NO SUCH FILE"
        val response = FileResponse(fileRequest, responseString)

        // when
        val file = response.file
        val anime = response.anime

        // then
        file.`should be null`()
        anime.`should be null`()
    }
}