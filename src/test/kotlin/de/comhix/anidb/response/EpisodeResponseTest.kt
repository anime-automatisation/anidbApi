package de.comhix.anidb.response

import de.comhix.anidb.data.EpisodeType.REGULAR
import de.comhix.anidb.data.episodeId
import kotlinx.datetime.Instant
import org.amshove.kluent.*
import org.testng.annotations.Test
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class EpisodeResponseTest {
    companion object {
        private val LOG = Logger.getLogger(EpisodeResponseTest::class.java.name)!!
    }

    @Test
    fun `parse episode response`() {
        LOG.info("parse episode response")

        // given
        val responseString = "240 EPISODE 1|1|24|400|4|01|Invasion|shinryaku|??"

        // when
        val episode = EpisodeResponse(responseString).episode

        // then
        episode.`should not be null`()
        episode.episodeId `should be equal to` 1
        episode.animeId `should be equal to` 1
        episode.length `should be equal to` 24
        episode.rating `should be equal to` 400
        episode.votes `should be equal to` 4
        episode.episodeNumber `should be equal to` 1
        episode.typedEpisodeNumber `should be equal to` "01"
        episode.englishName `should be equal to` "Invasion"
        episode.romajiName `should be equal to` "shinryaku"
        episode.kanjiName `should be equal to` "??"
    }

    @Test
    fun `parse episode response 2`() {
        LOG.info("parse episode response 2")

        // given
        val responseString = "240 EPISODE 2|1|24|750|2|02|Kin of the Stars|Hoshi-tachi no Kenzoku|??????|1295059229|1"

        // when
        val episode = EpisodeResponse(responseString).episode

        // then
        episode.`should not be null`()
        episode.episodeId `should be equal to` 2
        episode.animeId `should be equal to` 1
        episode.length `should be equal to` 24
        episode.rating `should be equal to` 750
        episode.votes `should be equal to` 2
        episode.episodeNumber `should be equal to` 2
        episode.typedEpisodeNumber `should be equal to` "02"
        episode.englishName `should be equal to` "Kin of the Stars"
        episode.romajiName `should be equal to` "Hoshi-tachi no Kenzoku"
        episode.kanjiName `should be equal to` "??????"
        episode.airedDate `should be equal to` Instant.fromEpochSeconds(1295059229)
        episode.episodeType `should be equal to` REGULAR
    }

    @Test
    fun `parse not existing episode`() {
        LOG.info("parse not existing episode")

        // given
        val responseString = "340 NO SUCH EPISODE"

        // when
        val episode = EpisodeResponse(responseString).episode

        // then
        episode.`should be null`()
    }
}