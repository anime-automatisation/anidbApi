package de.comhix.anidb.util

import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.logging.Logger.getLogger

/**
 * @author Benjamin Beeker
 */
object Ed2kChecksumTest {
    private val LOG = getLogger(Ed2kChecksumTest::class.java.name)

    @Test
    fun `ed2k zero`() {
        LOG.info("ed2k zero")

        // given
        val bytes = ByteArray(9728000)
        val zeroFile = File("zero.file")


        Files.write(Paths.get(zeroFile.toURI()), bytes)

        // when
        val sum = zeroFile.ed2kSum()

        // then
        val sumString = sum.joinToString(separator = "") { byte ->
            String.format("%02x", byte)
        }

        sumString `should be equal to` "fc21d9af828f92a8df64beac3357425d"
        Files.deleteIfExists(Paths.get(zeroFile.toURI()))
    }

    @Test
    fun `ed2k zero2`() {
        LOG.info("ed2k zero")

        // given
        val bytes = ByteArray(19456000)
        val zeroFile = File("zero.file")

        Files.write(Paths.get(zeroFile.toURI()), bytes)

        // when
        val sum = zeroFile.ed2kSum()

        // then
        val sumString = sum.joinToString(separator = "") { byte ->
            String.format("%02x", byte)
        }

        sumString `should be equal to` "114b21c63a74b6ca922291a11177dd5c"
        Files.deleteIfExists(Paths.get(zeroFile.toURI()))
    }

    @Test
    fun `ed2k file`() {
        LOG.info("ed2k file")

        // given
        val file = File("src/test/resources/anidb_fileid_1680995.mkv").absoluteFile

        // when
        val sum = file.ed2kSum()

        // then
        val sumString = sum.joinToString(separator = "") { byte ->
            String.format("%02x", byte)
        }

        sumString `should be equal to` "4379e96604daf117361416399a8c4bd7"
    }
}