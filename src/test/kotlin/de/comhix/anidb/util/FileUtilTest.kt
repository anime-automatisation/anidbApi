package de.comhix.anidb.util

import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.io.File
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class FileUtilTest {
    companion object {
        private val LOG = Logger.getLogger(FileUtilTest::class.java.name)!!
    }

    @Test
    fun `crc checksum`() {
        LOG.info("crc checksum")

        // given
        val file = File("src/test/resources/anidb_fileid_1680995.mkv").absoluteFile

        // when
        val checksum = file.crcString()

        // then
        checksum `should be equal to` "1e4974be"
    }
}