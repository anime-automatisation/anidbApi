package de.comhix.anidb.request

import de.comhix.anidb.request.mask.FileAnimeMask
import de.comhix.anidb.request.mask.MaskBuilder
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.util.logging.Logger.getLogger

/**
 * @author Benjamin Beeker
 */
object FileAnimeMaskTest {
    private val LOG = getLogger(FileAnimeMaskTest::class.java.name)

    @Test
    fun maskAll() {
        LOG.info("maskAll")

        // given
        val mask = MaskBuilder<FileAnimeMask>().all().build()

        // when
        val string = mask.toString()

        // then
        string `should be equal to` "fefcfcc1"
    }

    @Test
    fun `all flags set`() {
        LOG.info("all flags set")

        // given
        val mask = MaskBuilder<FileAnimeMask>()
                .set(FileAnimeMask::animeTotalEpisodes)
                .set(FileAnimeMask::highestEpisodeNumber)
                .set(FileAnimeMask::year)
                .set(FileAnimeMask::type)
                .set(FileAnimeMask::relatedAnimeIds)
                .set(FileAnimeMask::relatedAnimeTypes)
                .set(FileAnimeMask::categories)
                .set(FileAnimeMask::romajiName)
                .set(FileAnimeMask::kanjiName)
                .set(FileAnimeMask::englishName)
                .set(FileAnimeMask::otherName)
                .set(FileAnimeMask::shortNames)
                .set(FileAnimeMask::synonyms)
                .set(FileAnimeMask::episodeNumber)
                .set(FileAnimeMask::episodeName)
                .set(FileAnimeMask::episodeRomajiName)
                .set(FileAnimeMask::episodeKanjiName)
                .set(FileAnimeMask::episodeRating)
                .set(FileAnimeMask::episodeVoteCount)
                .set(FileAnimeMask::groupName)
                .set(FileAnimeMask::groupShortName)
                .set(FileAnimeMask::recordUpdatedDate)
                .build()

        // then
        mask.animeTotalEpisodes `should be equal to` true
        mask.highestEpisodeNumber `should be equal to` true
        mask.year `should be equal to` true
        mask.type `should be equal to` true
        mask.relatedAnimeIds `should be equal to` true
        mask.relatedAnimeTypes `should be equal to` true
        mask.categories `should be equal to` true
        mask.romajiName `should be equal to` true
        mask.kanjiName `should be equal to` true
        mask.englishName `should be equal to` true
        mask.otherName `should be equal to` true
        mask.shortNames `should be equal to` true
        mask.synonyms `should be equal to` true
        mask.episodeNumber `should be equal to` true
        mask.episodeName `should be equal to` true
        mask.episodeRomajiName `should be equal to` true
        mask.episodeKanjiName `should be equal to` true
        mask.episodeRating `should be equal to` true
        mask.episodeVoteCount `should be equal to` true
        mask.groupName `should be equal to` true
        mask.groupShortName `should be equal to` true
        mask.recordUpdatedDate `should be equal to` true
    }

    @Test
    fun `no flag set`() {
        LOG.info("no flag set")

        // given
        val mask = MaskBuilder<FileAnimeMask>().build()

        // then
        mask.animeTotalEpisodes `should be equal to` false
        mask.highestEpisodeNumber `should be equal to` false
        mask.year `should be equal to` false
        mask.type `should be equal to` false
        mask.relatedAnimeIds `should be equal to` false
        mask.relatedAnimeTypes `should be equal to` false
        mask.categories `should be equal to` false
        mask.romajiName `should be equal to` false
        mask.kanjiName `should be equal to` false
        mask.englishName `should be equal to` false
        mask.otherName `should be equal to` false
        mask.shortNames `should be equal to` false
        mask.synonyms `should be equal to` false
        mask.episodeNumber `should be equal to` false
        mask.episodeName `should be equal to` false
        mask.episodeRomajiName `should be equal to` false
        mask.episodeKanjiName `should be equal to` false
        mask.episodeRating `should be equal to` false
        mask.episodeVoteCount `should be equal to` false
        mask.groupName `should be equal to` false
        mask.groupShortName `should be equal to` false
        mask.recordUpdatedDate `should be equal to` false
    }
}