package de.comhix.anidb.request

import de.comhix.anidb.request.mask.FileMask
import de.comhix.anidb.request.mask.MaskBuilder
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.util.logging.Logger.getLogger

/**
 * @author Benjamin Beeker
 */
object FileMaskTest {
    private val LOG = getLogger(FileMaskTest::class.java.name)

    @Test
    fun testToString() {
        LOG.info("mask")

        // given
        val mask = MaskBuilder<FileMask>()
                .set(FileMask::animeId)
                .set(FileMask::fileType)
                .set(FileMask::crc32)
                .set(FileMask::audioCodecs)
                .build()

        // when
        val string = mask.toString()

        // then
        string `should be equal to` "4008210000"
    }

    @Test
    fun maskAll() {
        LOG.info("maskAll")

        // given
        val mask = MaskBuilder<FileMask>()
                .set(FileMask::animeId)
                .set(FileMask::episodeId)
                .set(FileMask::groupId)
                .set(FileMask::mylistId)
                .set(FileMask::otherEpisodes)
                .set(FileMask::deprecated)
                .set(FileMask::state)
                .set(FileMask::size)
                .set(FileMask::ed2k)
                .set(FileMask::md5)
                .set(FileMask::sha1)
                .set(FileMask::crc32)
                .set(FileMask::videoColorDepth)
                .set(FileMask::quality)
                .set(FileMask::source)
                .set(FileMask::audioCodecs)
                .set(FileMask::audioBitrates)
                .set(FileMask::videoCodec)
                .set(FileMask::videoBitrate)
                .set(FileMask::videoResolution)
                .set(FileMask::fileType)
                .set(FileMask::dubLanguage)
                .set(FileMask::subLanguage)
                .set(FileMask::lengthInSeconds)
                .set(FileMask::description)
                .set(FileMask::airedDate)
                .set(FileMask::anidbFileName)
                .set(FileMask::mylistState)
                .set(FileMask::mylistFilestate)
                .set(FileMask::mylistViewed)
                .set(FileMask::mylistViewdate)
                .set(FileMask::mylistStorage)
                .set(FileMask::mylistSource)
                .set(FileMask::mylistOther)
                .build()

        // when
        val string = mask.toString()

        // then
        string `should be equal to` "7ffafff9fe"
    }

    @Test
    fun `all flags set`() {
        LOG.info("all flags set")

        // given
        val mask = MaskBuilder<FileMask>().all().build()

        // then
        mask.animeId `should be equal to` true
        mask.episodeId `should be equal to` true
        mask.groupId `should be equal to` true
        mask.mylistId `should be equal to` true
        mask.otherEpisodes `should be equal to` true
        mask.deprecated `should be equal to` true
        mask.state `should be equal to` true
        mask.size `should be equal to` true
        mask.ed2k `should be equal to` true
        mask.md5 `should be equal to` true
        mask.sha1 `should be equal to` true
        mask.crc32 `should be equal to` true
        mask.videoColorDepth `should be equal to` true
        mask.quality `should be equal to` true
        mask.source `should be equal to` true
        mask.audioCodecs `should be equal to` true
        mask.audioBitrates `should be equal to` true
        mask.videoCodec `should be equal to` true
        mask.videoBitrate `should be equal to` true
        mask.videoResolution `should be equal to` true
        mask.fileType `should be equal to` true
        mask.dubLanguage `should be equal to` true
        mask.subLanguage `should be equal to` true
        mask.lengthInSeconds `should be equal to` true
        mask.description `should be equal to` true
        mask.airedDate `should be equal to` true
        mask.anidbFileName `should be equal to` true
        mask.mylistState `should be equal to` true
        mask.mylistFilestate `should be equal to` true
        mask.mylistViewed `should be equal to` true
        mask.mylistViewdate `should be equal to` true
        mask.mylistStorage `should be equal to` true
        mask.mylistSource `should be equal to` true
        mask.mylistOther `should be equal to` true
    }

    @Test
    fun `no flag set`() {
        LOG.info("no flag set")

        // given
        val mask = MaskBuilder<FileMask>().build()

        // then
        mask.animeId `should be equal to` false
        mask.episodeId `should be equal to` false
        mask.groupId `should be equal to` false
        mask.mylistId `should be equal to` false
        mask.otherEpisodes `should be equal to` false
        mask.deprecated `should be equal to` false
        mask.state `should be equal to` false
        mask.size `should be equal to` false
        mask.ed2k `should be equal to` false
        mask.md5 `should be equal to` false
        mask.sha1 `should be equal to` false
        mask.crc32 `should be equal to` false
        mask.videoColorDepth `should be equal to` false
        mask.quality `should be equal to` false
        mask.source `should be equal to` false
        mask.audioCodecs `should be equal to` false
        mask.audioBitrates `should be equal to` false
        mask.videoCodec `should be equal to` false
        mask.videoBitrate `should be equal to` false
        mask.videoResolution `should be equal to` false
        mask.fileType `should be equal to` false
        mask.dubLanguage `should be equal to` false
        mask.subLanguage `should be equal to` false
        mask.lengthInSeconds `should be equal to` false
        mask.description `should be equal to` false
        mask.airedDate `should be equal to` false
        mask.anidbFileName `should be equal to` false
        mask.mylistState `should be equal to` false
        mask.mylistFilestate `should be equal to` false
        mask.mylistViewed `should be equal to` false
        mask.mylistViewdate `should be equal to` false
        mask.mylistStorage `should be equal to` false
        mask.mylistSource `should be equal to` false
        mask.mylistOther `should be equal to` false
    }
}