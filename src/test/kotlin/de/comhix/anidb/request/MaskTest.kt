package de.comhix.anidb.request

import de.comhix.anidb.request.mask.*
import org.amshove.kluent.`should be true`
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.util.logging.Logger
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.full.memberProperties

/**
 * @author Benjamin Beeker
 */
class MaskTest {
    companion object {
        private val LOG = Logger.getLogger(MaskTest::class.java.name)!!
    }

    @DataProvider(name = "mask field provider")
    fun `mask field provider`(): Iterator<Array<Any>> {
        val testData: MutableList<Array<Any>> = mutableListOf()
        AnimeMask::class.memberProperties.filterIsInstance<KMutableProperty1<AnimeMask, Boolean>>()
                .forEach { testData.add(arrayOf(AnimeMask::class, it)) }
        FileMask::class.memberProperties.filterIsInstance<KMutableProperty1<AnimeMask, Boolean>>()
                .forEach { testData.add(arrayOf(FileMask::class, it)) }
        FileAnimeMask::class.memberProperties.filterIsInstance<KMutableProperty1<AnimeMask, Boolean>>()
                .forEach { testData.add(arrayOf(FileAnimeMask::class, it)) }
        return testData.asSequence().iterator()
    }

    @Test(dataProvider = "mask field provider")
    fun <MaskType : Mask> `mask getter and setter have same address`(maskClass: KClass<MaskType>, field: KMutableProperty1<MaskType, Boolean>) {
        LOG.info("mask getter and setter have same address")

        // given
        val mask = MaskBuilder(maskClass).set(field).build()

        // when
        val maskValue = field.get(mask)

        // then
        maskValue.`should be true`()
    }
}