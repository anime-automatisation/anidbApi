package de.comhix.anidb.request

import de.comhix.anidb.request.mask.AnimeMask
import de.comhix.anidb.request.mask.MaskBuilder
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class AnimeMaskTest {
    companion object {
        private val LOG = Logger.getLogger(AnimeMaskTest::class.java.name)!!
    }

    @Test
    fun `empty mask`() {
        LOG.info("empty mask")

        // given
        val builder = MaskBuilder<AnimeMask>()

        // when
        val mask = builder.build()

        // then
        mask.toString() `should be equal to` "00000000000000"
    }

    @Test
    fun `partially mask`() {
        LOG.info("partially mask")

        // given
        val builder = MaskBuilder<AnimeMask>().set(AnimeMask::animeId)

        // when
        val mask = builder.build()

        // then
        mask.toString() `should be equal to` "80000000000000"
    }

    @Test
    fun `full mask`() {
        LOG.info("full mask")

        // given
        val builder = MaskBuilder<AnimeMask>().all()

        // when
        val mask = builder.build()

        // then
        mask.toString() `should be equal to` "fcfcfeff7f80f8"
    }
}