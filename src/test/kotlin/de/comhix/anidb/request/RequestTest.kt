package de.comhix.anidb.request

import de.comhix.anidb.response.StringResponse
import org.amshove.kluent.`should be equal to`
import org.testng.annotations.Test
import java.util.logging.Logger

/**
 * @author Benjamin Beeker
 */
class RequestTest {
    companion object {
        private val LOG = Logger.getLogger(RequestTest::class.java.name)!!
    }

    @Test
    fun `new request without parameter`() {
        LOG.info("new request without parameter")

        // when
        val request = object : Request<StringResponse>("foobar") {}

        // then
        request.command `should be equal to` "foobar"
    }

    @Test
    fun `new request with one parameter`() {
        LOG.info("new request with one parameter")

        // when
        val request = object : Request<StringResponse>("foobar", Pair("a", "foo")) {}

        // then
        request.command `should be equal to` "foobar a=foo"
    }

    @Test
    fun `new request with multiple parameter`() {
        LOG.info("new request with multiple parameter")

        // when
        val request = object : Request<StringResponse>("foobar", Pair("a", "foo"), Pair("b", "bar")) {}

        // then
        request.command `should be equal to` "foobar a=foo&b=bar"
    }

    @Test
    fun `new request with null value parameter`() {
        LOG.info("new request with null value parameter")

        // when
        val request = object : Request<StringResponse>("foobar", Pair("a", null), Pair("b", "bar")) {}

        // then
        request.command `should be equal to` "foobar b=bar"
    }
}